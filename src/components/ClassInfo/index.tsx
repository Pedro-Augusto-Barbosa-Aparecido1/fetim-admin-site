import { theme } from "../../styles";
import { useSession } from "next-auth/react";
import { Text, Stack, Avatar } from "@chakra-ui/react";
import router from "next/router";

interface ClassProps {
  id: string;
  name: string;
  number: string;
  teacher: { id: string; name: string };
  subject: { id: string; name: string; abbreviation: string };
  student: Array<{ id: string; name: string }>;
}

export function ClassInfo({
  id,
  name,
  number,
  subject,
  teacher,
  student,
}: ClassProps) {
  const { status } = useSession();

  if (status === "authenticated") {
    return (
      <Stack
        h="70px"
        shadow="md"
        width="100%"
        color="white"
        direction="row"
        paddingLeft="3"
        paddingRight="3"
        cursor="pointer"
        borderRadius="10"
        alignItems="center"
        transitionDuration="250ms"
        backgroundColor={`${theme.colors.gray600}`}
        _hover={{
          transform: "scale(1.02)",
        }}
        onClick={() => {
          router.push({
            pathname: "classes/detail",
            query: {
              id,
              name,
              number,
              subject: subject ? subject.id.toString() : "",
              teacher: teacher ? teacher.id.toString() : "",
              student: student ? student.toString() : "",
              qtdStudents: student ? student.length : "0",
            },
          });
        }}
      >
        <Avatar />
        <Text width="40%" textAlign="center">
          {number}
        </Text>
        <Text width="40%" textAlign="center">
          {subject.abbreviation + " - " + subject.name}
        </Text>
        <Text noOfLines={2} width="100%" textAlign="center">
          {teacher ? teacher.name : "Sem professor"}
        </Text>
        <Text width="40%" textAlign="center">
          {student ? student.length : "0"}
        </Text>
      </Stack>
    );
  }

  return <></>;
}
