import { keyframes, styled } from "../../styles";
import { ArrowsClockwise } from "phosphor-react";

export const rotateAnimation = keyframes({
  from: {
    transform: "rotate(0deg)",
  },
  to: {
    transform: "rotate(360deg)",
  },
});

export const Spinner = styled(ArrowsClockwise, {
  animation: `${rotateAnimation} 800ms linear infinite`,
});
