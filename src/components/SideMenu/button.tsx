import { Button } from "@chakra-ui/react";

import { theme } from "../../styles";

import { useRouter } from "next/router";

interface SideMenuButtonProps {
  text: string;
  path: string;
}

export function SideMenuButton({ text, path }: SideMenuButtonProps) {
  const router = useRouter();

  function redirect() {
    router.push(path);
  }

  return (
    <Button
      variant="solid"
      color="white"
      backgroundColor={`${theme.colors.gray900}`}
      onClick={redirect}
      _hover={{
        border: "1px",
        backgroundColor: `${theme.colors.gray900}`,
      }}
    >
      {text}
    </Button>
  );
}
