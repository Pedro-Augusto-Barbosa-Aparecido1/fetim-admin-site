import {
  Flex,
  Stack,
  Drawer,
  Button,
  DrawerBody,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  useDisclosure,
  DrawerCloseButton,
} from "@chakra-ui/react";

import { theme } from "../../styles";
import { SideMenuButton } from "./button";
import { HamburgerIcon } from "@chakra-ui/icons";

import { UserInfo } from "../UserInfo";
import { SignOut } from "phosphor-react";
import { useSession, signOut } from "next-auth/react";

export function SideMenu() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { status } = useSession();

  const userIsAuthorized = status === "authenticated";

  return (
    <>
      <Button
        backgroundColor={`${theme.colors.green500}`}
        _hover={{ bg: `${theme.colors.green800}` }}
        size="md"
        leftIcon={<HamburgerIcon color="white" boxSize={6} />}
        iconSpacing={0}
        onClick={onOpen}
      ></Button>
      <Drawer placement="left" onClose={onClose} isOpen={isOpen}>
        <DrawerOverlay />
        <DrawerContent
          backgroundColor={`${theme.colors.gray800}`}
          color={`${theme.colors.white}`}
        >
          <DrawerCloseButton
            _hover={{ color: "red", bg: `${theme.colors.gray900}` }}
          />
          <DrawerHeader
            fontSize="2xl"
            borderBottom="1px"
            borderColor={`${theme.colors.gray600}`}
          >
            Academic Ace
          </DrawerHeader>
          <DrawerBody>
            <Stack height="100%">
              <Stack spacing="24px" mt="24px">
                {userIsAuthorized ? (
                  <>
                    <SideMenuButton text="Home" path="/home" />
                    <SideMenuButton text="Alunos" path="/system/students" />
                    <SideMenuButton
                      text="Professores"
                      path="/system/teachers"
                    />
                    <SideMenuButton text="Turmas" path="/system/classes" />
                    <SideMenuButton text="Matérias" path="/system/subjects" />
                    <SideMenuButton text="Provas" path="/system/tests" />
                    <SideMenuButton text="Cursos" path="/system/courses" />
                    <SideMenuButton text="Avisos" path="/system/annoucements" />
                    <SideMenuButton text="Suporte" path="/system/support" />
                  </>
                ) : (
                  <SideMenuButton text="Login" path="/login" />
                )}
              </Stack>
              <Flex
                height="100%"
                width="100%"
                mt="24px"
                mb="16px"
                alignItems="flex-end"
                justifyContent={{
                  base: "space-between",
                  sm: "space-between",
                  md: "space-between",
                  lg: "flex-end",
                }}
              >
                <UserInfo
                  name="Username"
                  role="Admin"
                  isSideMenu={true}
                ></UserInfo>
                <Button
                  display={{
                    base: "inline-flex",
                    sm: "inline-flex",
                    md: "inline-flex",
                    lg: "none",
                  }}
                  color="white"
                  paddingLeft="3"
                  paddingRight="0"
                  onClick={() => signOut()}
                  bg={`${theme.colors.gray800}`}
                  leftIcon={<SignOut size={28} weight="duotone" />}
                  _hover={{
                    color: "red",
                    bg: `${theme.colors.gray900}`,
                  }}
                ></Button>

                <Button
                  display={{
                    base: "none",
                    sm: "none",
                    md: "none",
                    lg: "inline-flex",
                  }}
                  width="100%"
                  color="white"
                  textAlign="center"
                  onClick={() => signOut()}
                  bg={`${theme.colors.gray800}`}
                  leftIcon={<SignOut size={28} weight="duotone" />}
                  _hover={{
                    color: "red",
                    bg: `${theme.colors.gray900}`,
                  }}
                >
                  Sair
                </Button>
              </Flex>
            </Stack>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
}
