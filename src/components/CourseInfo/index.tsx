import { theme } from "../../styles";
import { useSession } from "next-auth/react";
import { Text, Stack, Image } from "@chakra-ui/react";
import router from "next/router";

interface CourseProps {
  id: string;
  name: string;
}

export function CourseInfo({ id, name }: CourseProps) {
  const { status } = useSession();

  if (status === "authenticated") {
    return (
      <Stack
        h="70px"
        shadow="md"
        width="100%"
        color="white"
        direction="row"
        paddingLeft="3"
        paddingRight="3"
        cursor="pointer"
        borderRadius="10"
        alignItems="center"
        transitionDuration="250ms"
        backgroundColor={`${theme.colors.gray600}`}
        _hover={{
          transform: "scale(1.02)",
        }}
        onClick={() => {
          router.push({
            pathname: "courses/detail",
            query: {
              id,
              name,
            },
          });
        }}
      >
        <Image src="/logo.svg" height="50%" alt="" />
        <Text noOfLines={2} width="100%" textAlign="center">
          {name}
        </Text>
      </Stack>
    );
  }

  return <></>;
}
