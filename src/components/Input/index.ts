import { Text, TextInput } from "@ignite-ui/react";
import { styled } from "../../styles";

export const Input = styled(TextInput, {
  width: "100%",

  borderRadius: "$xs",
  height: 24,
});

export const InputError = styled(Text, {
  color: "$red600",

  marginTop: "$2",
});
