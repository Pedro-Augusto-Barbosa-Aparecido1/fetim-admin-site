import router from "next/router";
import { theme } from "../../styles";
import { useSession } from "next-auth/react";
import { Text, Stack, Image } from "@chakra-ui/react";

interface TestsProps {
  id: string;
  name: string;
  date: Date;
  classData: { id: string; name: string };
}

export function TestInfo({ id, name, date, classData }: TestsProps) {
  const { status } = useSession();

  const dateFormatted = new Date(date);

  if (status === "authenticated") {
    return (
      <Stack
        h="70px"
        shadow="md"
        width="100%"
        color="white"
        direction="row"
        paddingLeft="3"
        paddingRight="3"
        cursor="pointer"
        borderRadius="10"
        alignItems="center"
        transitionDuration="250ms"
        backgroundColor={`${theme.colors.gray600}`}
        _hover={{
          transform: "scale(1.02)",
        }}
        onClick={() => {
          router.push({
            pathname: "tests/detail",
            query: {
              id,
              name,
              date: dateFormatted.toLocaleDateString(),
              classId: classData ? classData.id : "",
            },
          });
        }}
      >
        <Image src="/logo.svg" height="50%" alt="" />
        <Text noOfLines={2} width="50%" textAlign="center">
          {name}
        </Text>
        <Text noOfLines={2} width="50%" textAlign="center">
          {classData ? classData.name : "Sem turma"}
        </Text>
        <Text noOfLines={2} width="50%" textAlign="center">
          {dateFormatted.toLocaleDateString()}
        </Text>
      </Stack>
    );
  }

  return <></>;
}
