import { theme } from "../../styles";
import { useRouter } from "next/router";
import { Button } from "@chakra-ui/react";

export function CancelButton() {
  const router = useRouter();

  function redirect() {
    router.back();
  }

  return (
    <Button
      width="100%"
      color="white"
      onClick={redirect}
      backgroundColor={`${theme.colors.red700}`}
      _hover={{
        backgroundColor: `${theme.colors.red900}`,
        color: "gray600",
      }}
    >
      Cancelar
    </Button>
  );
}
