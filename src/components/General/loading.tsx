import { Flex, Spinner } from "@chakra-ui/react";

export function Loading() {
  return (
    <Flex
      position="absolute"
      top="0"
      left="0"
      width="100%"
      height="100%"
      backgroundColor="rgba(0, 0, 0, 0.5)"
      zIndex={250}
      alignItems="center"
      justifyContent="center"
    >
      <Spinner
        thickness="6px"
        speed="0.75s"
        emptyColor="gray.200"
        color="green.500"
        size="xl"
      />
    </Flex>
  );
}
