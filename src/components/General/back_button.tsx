import { theme } from "../../styles";
import { useRouter } from "next/router";
import { Button } from "@chakra-ui/react";

export function BackButton() {
  const router = useRouter();

  function redirect() {
    router.back();
  }

  return (
    <Button
      width="100%"
      type="submit"
      color="white"
      onClick={redirect}
      backgroundColor={`${theme.colors.green500}`}
      _hover={{
        backgroundColor: `${theme.colors.green800}`,
        color: "gray600",
      }}
    >
      Voltar
    </Button>
  );
}
