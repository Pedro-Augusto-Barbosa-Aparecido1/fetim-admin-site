import { theme } from "../../styles";
import { useRouter } from "next/router";
import { Button } from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";

interface CreateButtonProps {
  path: string;
}

export function CreateButton({ path }: CreateButtonProps) {
  const router = useRouter();

  function redirect() {
    router.push(path);
  }

  return (
    <Button
      width="100%"
      type="submit"
      color="white"
      ml="2"
      onClick={redirect}
      leftIcon={<AddIcon />}
      backgroundColor={`${theme.colors.green500}`}
      _hover={{
        backgroundColor: `${theme.colors.green800}`,
        color: "gray600",
      }}
    >
      Create
    </Button>
  );
}
