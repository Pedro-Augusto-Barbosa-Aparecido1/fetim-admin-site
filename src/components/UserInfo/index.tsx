import { Box, Text, Wrap, Avatar, WrapItem } from "@chakra-ui/react";

import { useSession } from "next-auth/react";

interface UserProps {
  name: string;
  role: string;
  isSideMenu: boolean;
}

export function UserInfo({ name, role, isSideMenu }: UserProps) {
  const { status, data } = useSession();

  if (status === "authenticated") {
    return (
      <Wrap
        display={
          isSideMenu
            ? { base: "flex", sm: "flex", md: "flex", lg: "none" }
            : { base: "none", sm: "none", md: "none", lg: "flex" }
        }
      >
        <WrapItem>
          <Avatar name="" src="#" />
        </WrapItem>
        <WrapItem alignItems="center">
          <Box ml="2">
            <Text fontWeight="bold" color="white">
              {data.user?.name ?? name}
            </Text>
            <Text color="white">{role}</Text>
          </Box>
        </WrapItem>
      </Wrap>
    );
  }

  return <></>;
}
