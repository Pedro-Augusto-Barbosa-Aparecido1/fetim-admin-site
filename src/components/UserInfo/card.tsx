import { Card, Text, Avatar, CardBody, CardFooter } from "@chakra-ui/react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { theme } from "../../styles";

interface UserProps {
  id: string;
  img: string;
  name: string;
  email: string;
  active: string;
  classes?: string[];
  course: { name: string; id: string };
  pathname?: string;
}

export function UserCard({
  id,
  name,
  email,
  active,
  course,
  pathname = "students/detail",
  classes,
}: UserProps) {
  const { status } = useSession();
  const router = useRouter();

  if (status === "authenticated") {
    return (
      <Card
        maxHeight="284"
        boxShadow="dark-lg"
        cursor="pointer"
        borderRadius="10"
        transitionDuration="250ms"
        backgroundColor={`${theme.colors.gray600}`}
        _hover={{
          transform: "scale(1.02)",
        }}
        onClick={() => {
          router.push({
            pathname,
            query: {
              id,
              name,
              email,
              active,
              courseId: course.id,
              classes,
            },
          });
        }}
      >
        <CardBody display="flex" justifyContent="center">
          <Avatar
            name=""
            src="#"
            boxSize="150"
            showBorder={true}
            border={`2px solid ${theme.colors.gray700}`}
          />
        </CardBody>
        <CardFooter
          color="white"
          display="flex"
          alignItems="center"
          flexDirection="column"
          justifyContent="center"
          borderBottomRadius="8px"
          backgroundColor={`${theme.colors.green500}`}
        >
          <Text
            width="100%"
            title={name}
            noOfLines={1}
            fontSize="xl"
            textAlign="center"
            fontWeight="bold"
          >
            {name}
          </Text>
          <Text noOfLines={1}>{course.name}</Text>
        </CardFooter>
      </Card>
    );
  }

  return <></>;
}
