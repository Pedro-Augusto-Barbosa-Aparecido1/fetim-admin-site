import axios from "axios";

export const api = axios.create({
  baseURL: "/api",
});

export const backendApi = axios.create({
  baseURL: process.env.BACKEND_URL,
});
