import { styled } from "@ignite-ui/react";

/* -------------- Container -------------- */
export const Container = styled("div", {
  width: "100%",
  height: "100vh",

  display: "flex",
  flexDirection: "column",
  backgroundColor: "$gray800",
});

/* -------------- Header -------------- */
export const Header = styled("header", {
  width: "100%",
  padding: "15px",
  display: "flex",
  alignItems: "center",
  position: "fixed",
  zIndex: 500,
  justifyContent: "space-between",
  backgroundColor: "$green500",
});

// Header Title
export const HeaderTitle = styled("h1", {
  color: "$white",
  fontSize: "$4xl",
  position: "absolute",
  fontWeight: "bold",
  left: "50%",
  transform: "translateX(-50%)",
});
