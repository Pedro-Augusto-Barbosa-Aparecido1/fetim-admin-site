import { Center, Image, Text, Box, ScaleFade } from "@chakra-ui/react";

import Link from "next/link";

export default function NotFound() {
  return (
    <>
      <Center width="100%" height="100%">
        <ScaleFade delay={0.1} initialScale={0.1} in={true}>
          <Box>
            <Box display="flex" alignItems="center" justifyContent="center">
              <Image src="/logo.svg" alt="logo" boxSize="48" />
            </Box>
            <Box
              display="flex"
              alignItems="center"
              justifyContent="center"
              width="lg"
            >
              <Text
                color="white"
                fontSize="2xl"
                textAlign="center"
                fontWeight="bold"
              >
                Unfortunately the page does not exist or you do not have access,{" "}
                <Link href="/home" style={{ color: "Highlight" }}>
                  back to home.
                </Link>
              </Text>
            </Box>
          </Box>
        </ScaleFade>
      </Center>
    </>
  );
}
