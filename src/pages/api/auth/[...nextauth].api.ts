import CredentialsProvider from "next-auth/providers/credentials";
import NextAuth, { NextAuthOptions } from "next-auth";

import { prisma } from "../../../lib/prisma";

import { signInWithEmailAndPassword, getAuth } from "firebase/auth";
import { PrismaAdapter } from "@next-auth/prisma-adapter";

import "../../../lib/firebase";

export const authOptions: NextAuthOptions = {
  session: {
    strategy: "jwt",
  },

  adapter: PrismaAdapter(prisma),
  providers: [
    CredentialsProvider({
      credentials: {
        email: {
          type: "email",
          label: "E-Mail",
        },
        password: {
          type: "password",
          label: "Password",
        },
      },
      authorize: async (credentials, request) => {
        if (!request.body?.email || !request.body?.password) {
          throw new Error("Invalid credential 1");
        }

        const user = await prisma.user.findUnique({
          where: {
            email: request.body.email,
          },
        });

        if (!user) {
          throw new Error("User not found");
        }

        try {
          const auth = await getAuth();
          await signInWithEmailAndPassword(
            auth,
            request.body.email,
            request.body.password
          );
        } catch (e: any) {
          throw new Error(e.message);
        }

        return {
          id: user.id,
          email: user.email,
          image: user.image,
          name: user.name,
        };
      },
    }),
  ],
  pages: {
    signIn: "/login",
  },
};

export default NextAuth(authOptions);
