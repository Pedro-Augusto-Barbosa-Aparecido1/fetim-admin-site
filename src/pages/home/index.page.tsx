import { Center, Image, Text, Box, ScaleFade } from "@chakra-ui/react";
import type { NextPage } from "next";

const Home: NextPage = () => {
  return (
    <Center width="100%" height="100%">
      <ScaleFade delay={0.1} initialScale={0.1} in={true}>
        <Box>
          <Box display="flex" alignItems="center" justifyContent="center">
            <Image src="/logo.svg" alt="logo" boxSize="48" />
          </Box>
          <Box display="flex" alignItems="center" justifyContent="center">
            <Text color="white" fontSize="6xl" fontWeight="bold">
              Academic Ace
            </Text>
          </Box>
        </Box>
      </ScaleFade>
    </Center>
  );
};

export default Home;
