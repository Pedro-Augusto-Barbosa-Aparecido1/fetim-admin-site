import {
  Text,
  Flex,
  Input,
  Button,
  Center,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
  Stack,
} from "@chakra-ui/react";
import type { NextPage } from "next";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import axios, { AxiosError } from "axios";
import { theme } from "../../../../styles";
import { Controller, useForm } from "react-hook-form";
import { ShieldSlash } from "phosphor-react";
import { useRouter } from "next/router";
import { Loading } from "../../../../components/General/loading";
import { CancelButton } from "../../../../components/General/cancel_button";
import { useState } from "react";

const subjectDataSchema = z.object({
  id: z.string(),
  name: z.string(),
  abbreviation: z.string(),
});

type SubjecData = z.infer<typeof subjectDataSchema>;

const ClassCreate: NextPage = () => {
  const toast = useToast();
  const router = useRouter();
  const [isLoading, setLoading] = useState(false);

  const {
    register,
    control,
    getValues,
    formState: { isSubmitting, errors },
  } = useForm<SubjecData>({
    resolver: zodResolver(subjectDataSchema),
  });

  const redirect = () => {
    router.push("/system/subjects");
  };

  async function createSubject() {
    const data = getValues();

    try {
      setLoading(true);
      await axios.post(
        "https://academic-ace-backend.vercel.app/subjects/create",
        data
      );

      toast({
        title: "Matéria criada com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;
      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao criar Matéria",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}
      <Center width="100%" marginTop={10}>
        <Flex
          width="50%"
          height="100%"
          alignItems="center"
          flexDirection="column"
          margin={4}
        >
          <Text color="white" fontSize="4xl" fontWeight="bold">
            Criar Matéria
          </Text>
          <Flex width="100%">
            <FormControl>
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Matéria..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message && (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Sigla</FormLabel>
              <Controller
                name="abbreviation"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Sigla..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        {...register("abbreviation")}
                      />
                      {errors.abbreviation?.message && (
                        <FormHelperText color="red">
                          {errors.abbreviation.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <Stack direction="row" marginTop={5}>
                <CancelButton />
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  isLoading={isSubmitting || isLoading}
                  onClick={createSubject}
                >
                  Cadastrar
                </Button>
              </Stack>
            </FormControl>
          </Flex>
        </Flex>
      </Center>
    </>
  );
};

export default ClassCreate;
