import {
  Text,
  Flex,
  Input,
  Button,
  Center,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";
import type { NextPage } from "next";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import axios, { AxiosError } from "axios";
import { theme } from "../../../../styles";
import { Controller, useForm } from "react-hook-form";
import { BackButton } from "../../../../components/General/back_button";
import { ShieldSlash } from "phosphor-react";
import { useRouter } from "next/router";
import { Loading } from "../../../../components/General/loading";
import { useState } from "react";

interface RouterQuery {
  id: string;
  name: string;
  qtdClasses: string;
  abbreviation: string;
}

const subjectDataSchema = z.object({
  id: z.string(),
  name: z.string(),
  abbreviation: z.string(),
});

type SubjecData = z.infer<typeof subjectDataSchema>;

const SubjectDetail: NextPage = () => {
  const toast = useToast();
  const router = useRouter();
  const [isLoading, setLoading] = useState(false);

  const { id, name, qtdClasses, abbreviation } =
    router.query as unknown as RouterQuery;

  const {
    register,
    control,
    getValues,
    formState: { isSubmitting, errors },
  } = useForm<SubjecData>({
    resolver: zodResolver(subjectDataSchema),
    defaultValues: {
      id,
      name,
      abbreviation,
    },
  });

  const redirect = () => {
    router.push("/system/subjects");
  };

  async function updateClass() {
    const data = getValues();
    try {
      setLoading(true);
      await axios.put(
        "https://academic-ace-backend.vercel.app/subjects/update/",
        data
      );
      toast({
        title: "Matéria atualizada com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao atualizar matéria",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  async function deleteClass() {
    try {
      setLoading(true);
      await axios.delete(
        `https://academic-ace-backend.vercel.app/subjects/delete/${id}`,
        {
          params: { id },
        }
      );
      toast({
        title: "Matéria removida com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao remover matéria",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}
      <Center width="100%" marginTop={10}>
        <Flex
          width="50%"
          height="100%"
          alignItems="center"
          flexDirection="column"
          margin={4}
        >
          <Text color="white" fontSize="4xl" fontWeight="bold">
            Atualizar Matéria
          </Text>
          <Flex width="100%">
            <FormControl>
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Matéria..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message && (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Sigla</FormLabel>
              <Controller
                name="abbreviation"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Sigla..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        {...register("abbreviation")}
                      />
                      {errors.abbreviation?.message && (
                        <FormHelperText color="red">
                          {errors.abbreviation.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Quantidade de turmas</FormLabel>
              <Input
                placeholder="Qtd turmas..."
                color="white"
                marginBottom="3"
                value={qtdClasses}
                disabled={true}
              />
            </FormControl>
          </Flex>
        </Flex>
      </Center>
      <Flex
        width="100%"
        padding={4}
        direction="row"
        justifyContent="space-between"
        position="fixed"
        bottom={0}
        id="page-footer"
      >
        <Flex width={32}>
          <BackButton></BackButton>
        </Flex>

        <Flex width={72} justifyContent="space-between">
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={`${theme.colors.red700}`}
            _hover={{
              backgroundColor: `${theme.colors.red900}`,
              color: "gray600",
            }}
            isLoading={isSubmitting || isLoading}
            onClick={deleteClass}
          >
            Deletar
          </Button>
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={"blue.500"}
            _hover={{
              backgroundColor: `blue.700`,
              color: "gray600",
            }}
            isLoading={isSubmitting || isLoading}
            onClick={updateClass}
          >
            Atualizar
          </Button>
        </Flex>
      </Flex>
    </>
  );
};

export default SubjectDetail;
