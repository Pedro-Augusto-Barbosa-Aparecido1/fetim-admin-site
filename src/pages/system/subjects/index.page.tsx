import {
  Text,
  Flex,
  Stack,
  Input,
  Button,
  FormLabel,
  FormControl,
  Avatar,
  FormHelperText,
} from "@chakra-ui/react";
import { NextPage } from "next";
import { theme } from "../../../styles";
import { SearchIcon } from "@chakra-ui/icons";
import { z } from "zod";
import axios from "axios";
import { zodResolver } from "@hookform/resolvers/zod";
import { SubjectInfo } from "../../../components/SubjectInfo";
import { BackButton } from "../../../components/General/back_button";
import { CreateButton } from "../../../components/General/create_button";
import { useState, useEffect } from "react";
import { Controller, useForm } from "react-hook-form";
import { Loading } from "../../../components/General/loading";

const subjectsDataSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    abbreviation: z.string(),
    classes: z.array(z.object({ id: z.string(), name: z.string() })),
  })
);

type ClassData = z.infer<typeof subjectsDataSchema>;

const filterSchema = z.object({
  name: z.string(),
  abbreviation: z.string(),
  page: z.string(),
  limit: z.string(),
});

type FilterData = z.infer<typeof filterSchema>;

const Classes: NextPage = () => {
  const [subjects, setSubjects] = useState<ClassData>();
  const [isLoading, setLoading] = useState(true);

  const {
    register,
    control,
    getValues,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<FilterData>({
    resolver: zodResolver(filterSchema),
    defaultValues: {
      name: "",
      abbreviation: "",
      page: "1",
      limit: "100",
    },
  });

  async function fetchSubjects() {
    const filters = getValues();

    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/subjects/list",
        {
          params: { ...filters },
        }
      );

      setSubjects(response.data.subjects);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchSubjects();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isSubmitting || isLoading ? <Loading /> : <></>}
      <Flex marginLeft="10" marginTop="10" marginRight="10">
        <FormControl>
          <Stack
            direction={{
              base: "column",
              sm: "column",
              md: "row",
              lg: "row",
            }}
          >
            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "75%", lg: "75%" }}
            >
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Nome..."
                        color="white"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message ? (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      ) : (
                        <FormHelperText />
                      )}
                    </>
                  );
                }}
              ></Controller>
            </Stack>

            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "75%", lg: "75%" }}
            >
              <FormLabel color="white">Sigla</FormLabel>
              <Controller
                name="abbreviation"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Sigla..."
                        color="white"
                        value={field.value}
                        {...register("abbreviation")}
                      />
                      {errors.abbreviation?.message ? (
                        <FormHelperText color="red">
                          {errors.abbreviation.message}
                        </FormHelperText>
                      ) : (
                        <FormHelperText />
                      )}
                    </>
                  );
                }}
              ></Controller>
            </Stack>

            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "25%" }}
              mt={{ base: "5", sm: "5", md: "5", lg: "0" }}
              mb={{ base: "5", sm: "5", md: "5", lg: "0" }}
            >
              <FormLabel
                opacity="0"
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              >
                Ignore this
              </FormLabel>
              <Flex alignItems="flex-end">
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  leftIcon={<SearchIcon />}
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  onClick={handleSubmit(fetchSubjects)}
                >
                  Search
                </Button>
                <CreateButton path="subjects/create" />
              </Flex>
              <FormHelperText
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              />
            </Stack>
          </Stack>
        </FormControl>
      </Flex>
      <Stack
        h="30px"
        marginTop="10"
        paddingLeft="3"
        direction="row"
        marginLeft="10"
        marginRight="10"
        paddingRight="3"
        borderRadius="10"
        alignItems="center"
        color={`${theme.colors.green500}`}
      >
        <Avatar
          zIndex="-100"
          icon={<></>}
          variant="ghost"
          backgroundColor={`${theme.colors.gray800}`}
        />
        <Text fontSize="lg" width="50%" textAlign="center" fontWeight="bold">
          Sigla
        </Text>
        <Text fontSize="lg" width="50%" textAlign="center" fontWeight="bold">
          Nome
        </Text>
        <Text fontSize="lg" width="50%" textAlign="center" fontWeight="bold">
          Quantidade de Turmas
        </Text>
      </Stack>
      <Stack marginLeft="10" marginRight="10" spacing="3" zIndex="1">
        {isSubmitting ? (
          <></>
        ) : (
          subjects &&
          subjects.map((subject, index) => (
            <SubjectInfo
              id={subject.id}
              key={subject.id}
              name={subject.name}
              abbreviation={subject.abbreviation}
              classes={subject.classes}
            />
          ))
        )}
      </Stack>
      <Stack width="32" margin={4} position="fixed" bottom={0} id="page-footer">
        <BackButton />
      </Stack>
    </>
  );
};

export default Classes;
