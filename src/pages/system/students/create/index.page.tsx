import {
  Flex,
  Input,
  Stack,
  Button,
  Select,
  Center,
  Avatar,
  LinkBox,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";

import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import type { NextPage } from "next";
import axios, { AxiosError } from "axios";
import { useEffect, useState } from "react";

import { useRouter } from "next/router";

import { ShieldSlash } from "phosphor-react";
import { Controller, useForm } from "react-hook-form";

import { theme } from "../../../../styles";
import { CancelButton } from "../../../../components/General/cancel_button";

const studentsformSchema = z.object({
  name: z.string(),
  email: z.string().email(),
  course: z.string(),
});

type studentsFormData = z.infer<typeof studentsformSchema>;

const coursesGetSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    page: z.string(),
    total: z.string(),
  })
);

type CoursesGetData = z.infer<typeof coursesGetSchema>;

const StudentsCreate: NextPage = () => {
  const toast = useToast();
  const [courses, setCourses] = useState<CoursesGetData>();
  const [isLoading, setLoading] = useState(true);
  const router = useRouter();

  const {
    register,
    control,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<studentsFormData>({
    resolver: zodResolver(studentsformSchema),
    defaultValues: {
      name: "",
      email: "",
      course: "",
    },
  });

  const redirect = () => {
    router.push("/system/students");
  };

  async function fetchCourses() {
    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/courses/list",
        {
          params: { page: 1, limit: 100 },
        }
      );

      setCourses(response.data.courses);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  async function createStudent(data: studentsFormData) {
    try {
      await axios.post(
        "https://academic-ace-backend.vercel.app/students/register",
        data
      );

      toast({
        title: "Estudante criado com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;
      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao criar estudante",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    }
  }

  useEffect(() => {
    fetchCourses();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Center width="100%" height="100%">
        <Flex width="xl" flexDirection="column" marginLeft="5" marginRight="5">
          <Flex
            alignItems="center"
            marginBottom="25px"
            flexDirection="column"
            justifyContent="space-arroud"
          >
            <LinkBox>
              <Avatar
                name=""
                src="#"
                boxSize="150"
                cursor="pointer"
                showBorder={true}
                border={`2px solid ${theme.colors.gray700}`}
                _hover={{
                  opacity: "0.6",
                }}
              />
            </LinkBox>
          </Flex>

          <Flex alignItems="center">
            <FormControl>
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Nome..."
                        color="white"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message ? (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      ) : (
                        <FormHelperText />
                      )}
                    </>
                  );
                }}
              />

              <FormLabel color="white" marginTop="1">
                Email
              </FormLabel>
              <Controller
                name="email"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Email..."
                        color="white"
                        value={field.value}
                        {...register("email")}
                      />
                      {errors.email?.message ? (
                        <FormHelperText color="red">
                          {errors.email.message}
                        </FormHelperText>
                      ) : (
                        <FormHelperText />
                      )}
                    </>
                  );
                }}
              />

              <FormLabel color="white" marginTop="1">
                Curso
              </FormLabel>
              <Controller
                name="course"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        placeholder="Curso..."
                        color="white"
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        value={field.value}
                        {...register("course")}
                      >
                        {isLoading ? (
                          <></>
                        ) : (
                          courses &&
                          courses.map((option) => (
                            <option key={option.id} value={option.id}>
                              {option.name}
                            </option>
                          ))
                        )}
                      </Select>
                      {errors.course?.message ? (
                        <FormHelperText color="red">
                          {errors.course.message}
                        </FormHelperText>
                      ) : (
                        <FormHelperText />
                      )}
                    </>
                  );
                }}
              />

              <Stack direction="row" marginTop={5}>
                <CancelButton />
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  isLoading={isSubmitting}
                  onClick={handleSubmit(createStudent)}
                >
                  Cadastrar
                </Button>
              </Stack>
            </FormControl>
          </Flex>
        </Flex>
      </Center>
    </>
  );
};

export default StudentsCreate;
