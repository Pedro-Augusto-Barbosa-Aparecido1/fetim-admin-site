import {
  Flex,
  Stack,
  Input,
  Button,
  Select,
  useToast,
  FormLabel,
  SimpleGrid,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";

import axios from "axios";
import type { NextPage } from "next";
import { Controller, useForm } from "react-hook-form";
import { useEffect, useState } from "react";

import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import { theme } from "../../../styles";
import { ShieldSlash } from "phosphor-react";
import { SearchIcon } from "@chakra-ui/icons";
import { UserCard } from "../../../components/UserInfo/card";
import { BackButton } from "../../../components/General/back_button";
import { CreateButton } from "../../../components/General/create_button";
import { Loading } from "../../../components/General/loading";

const studentsFilterSchema = z.object({
  name: z.string(),
  email: z
    .string()
    .refine(
      (value) =>
        value === "" || /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value),
      {
        message: "Email inválido",
      }
    ),
  courseId: z.string(),
  classId: z.string(),
  active: z.string(),
  limit: z.string(),
  page: z.string(),
});

type StudentsFilterData = z.infer<typeof studentsFilterSchema>;

const studentsformSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    email: z.string().email(),
    active: z.string(),
    course: z.object({ id: z.string(), name: z.string() }),
    classes: z.array(z.string()),
  })
);

type StudentsFormData = z.infer<typeof studentsformSchema>;

const coursesGetSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    page: z.string(),
    total: z.string(),
  })
);

type CoursesGetData = z.infer<typeof coursesGetSchema>;

const Students: NextPage = () => {
  const [students, setStudents] = useState<StudentsFormData>();
  const [courses, setCourses] = useState<CoursesGetData>();
  const [isLoading, setLoading] = useState(true);
  const toast = useToast();

  const {
    register,
    control,
    getValues,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<StudentsFilterData>({
    resolver: zodResolver(studentsFilterSchema),
    defaultValues: {
      name: "",
      email: "",
      page: "1",
      limit: "10",
      classId: "",
      courseId: "",
      active: "true",
    },
  });

  async function fetchStudents() {
    const filters = getValues();

    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/students/",
        {
          params: filters,
        }
      );

      setStudents(
        response.data.students?.map((student: any) => ({
          ...student,
          classes: student.classes?.map((classe: any) => classe.id),
        }))
      );
    } catch (error) {
      toast({
        title: "Erro ao listar estudantes",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  async function fetchCourses() {
    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/courses/list",
        {
          params: { page: 1, limit: 100 },
        }
      );

      setCourses(response.data.courses);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchStudents();
    fetchCourses();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}

      {/* ------ Filters ------ */}
      <Flex marginLeft="10" marginTop="10" marginRight="10">
        <FormControl>
          <Stack
            direction={{
              base: "column",
              sm: "column",
              md: "column",
              lg: "row",
            }}
          >
            <Stack spacing="0" width="100%">
              <FormLabel color="white">Email</FormLabel>
              <Controller
                name="email"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Email..."
                        color="white"
                        value={field.value}
                        {...register("email")}
                      />
                      {errors.email?.message && (
                        <FormHelperText color="red">
                          {errors.email.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </Stack>
            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "75%" }}
            >
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Nome..."
                        color="white"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message && (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </Stack>
            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "50%" }}
            >
              <FormLabel color="white">Curso</FormLabel>
              <Controller
                name="courseId"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        placeholder="Curso..."
                        color="white"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("courseId")}
                      >
                        {isLoading ? (
                          <></>
                        ) : (
                          courses &&
                          courses.map((option) => (
                            <option key={option.id} value={option.id}>
                              {option.name}
                            </option>
                          ))
                        )}
                      </Select>
                      {errors.courseId?.message && (
                        <FormHelperText color="red">
                          {errors.courseId.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </Stack>
            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "50%" }}
            >
              <FormLabel color="white">Turma</FormLabel>
              <Controller
                name="classId"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        placeholder="Turma..."
                        color="white"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("classId")}
                      />
                      {errors.classId?.message && (
                        <FormHelperText color="red">
                          {errors.classId.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </Stack>

            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "50%" }}
            >
              <FormLabel color="white">Ativo</FormLabel>
              <Controller
                name="active"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        color="white"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("active")}
                      >
                        <option value="true">Ativo</option>
                        <option value="false">Desativo</option>
                      </Select>
                      {errors.active?.message && (
                        <FormHelperText color="red">
                          {errors.active.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </Stack>

            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "50%" }}
              mt={{ base: "5", sm: "5", md: "5", lg: "0" }}
              mb={{ base: "5", sm: "5", md: "5", lg: "0" }}
            >
              <FormLabel
                opacity="0"
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              >
                Ignore this
              </FormLabel>
              <Flex alignItems="flex-end">
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  leftIcon={<SearchIcon />}
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  isLoading={isSubmitting}
                  loadingText=""
                  onClick={handleSubmit(fetchStudents)}
                >
                  Search
                </Button>
                <CreateButton path="students/create" />
              </Flex>
              <FormHelperText
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              />
            </Stack>
          </Stack>
        </FormControl>
      </Flex>

      {/* ------ Student List ------ */}
      <SimpleGrid
        margin="10"
        marginTop="5"
        spacing={6}
        templateColumns="repeat(auto-fill, minmax(250px, 1fr))"
      >
        {isSubmitting ? (
          <></>
        ) : (
          students &&
          students.map((student, index) => (
            <UserCard
              id={student.id}
              img=""
              name={student.name}
              email={student.email}
              course={student.course}
              active={student.active}
              classes={student.classes}
              key={student.id}
            />
          ))
        )}
      </SimpleGrid>
      <Stack width="32" margin={4} position="fixed" bottom={0} id="page-footer">
        <BackButton />
      </Stack>
    </>
  );
};

export default Students;
