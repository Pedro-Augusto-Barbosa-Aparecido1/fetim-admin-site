import {
  Flex,
  Grid,
  Input,
  Button,
  Select,
  Center,
  Avatar,
  LinkBox,
  useToast,
  GridItem,
  FormLabel,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";

import { MultiSelect, Option } from "chakra-multiselect";

import type { NextPage } from "next";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import axios, { AxiosError } from "axios";
import { theme } from "../../../../styles";
import { Controller, useForm } from "react-hook-form";
import { BackButton } from "../../../../components/General/back_button";
import { ShieldSlash } from "phosphor-react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Loading } from "../../../../components/General/loading";

const studentsformSchema = z.object({
  id: z.string(),
  name: z.string(),
  // classe: z.array(z.string()),
  email: z
    .string()
    .refine(
      (value) =>
        value === "" || /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value),
      {
        message: "Email inválido",
      }
    ),
  active: z.string(),
  course_id: z.string(),
});

type studentsFormData = z.infer<typeof studentsformSchema>;

const coursesGetSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    page: z.string(),
    total: z.string(),
  })
);

type ClassesReponse = {
  id: string;
  name: string;
};

type coursesGetData = z.infer<typeof coursesGetSchema>;

const StudentsDetail: NextPage = () => {
  const router = useRouter();
  let { id, name, email, active, courseId, classes: classeIds } = router.query;

  const toast = useToast();
  const [courses, setCourses] = useState<coursesGetData>();
  const [classes, setClasses] = useState<ClassesReponse[]>([]);
  const [classesSelected, setClassesSelected] = useState<Option[]>([]);
  const [isLoading, setLoading] = useState(true);

  // Ensure that the values are string
  id = id ? (Array.isArray(id) ? id[0] : id) : "";
  name = name ? (Array.isArray(name) ? name[0] : name) : "";
  email = email ? (Array.isArray(email) ? email[0] : email) : "";
  active = active ? (Array.isArray(active) ? active[0] : active) : "";
  courseId = courseId ? (Array.isArray(courseId) ? courseId[0] : courseId) : "";
  classeIds = classeIds
    ? Array.isArray(classeIds)
      ? (classeIds as Array<string>)
      : ([classeIds] as Array<string>)
    : ([] as Array<string>);

  const {
    register,
    control,
    setValue,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<studentsFormData>({
    resolver: zodResolver(studentsformSchema),
    defaultValues: {
      id,
      name,
      email,
      active,
      course_id: courseId,
    },
  });

  useEffect(() => {
    // Set the default value for courseId after fetching courses
    setValue(
      "course_id",
      courseId ? (Array.isArray(courseId) ? courseId[0] : courseId) : ""
    );
  }, [courseId, setValue]);

  const redirect = () => {
    router.push("/system/students");
  };

  async function updateStudent(data: studentsFormData) {
    const classesToSet: string[] = classesSelected.reduce((acc, option) => {
      const optionId = classes.find((classe) => classe.name === option.value);

      if (optionId && !acc.some((o) => o === optionId.id)) {
        acc.push(optionId.id);
      }

      return acc;
    }, [] as string[]);

    try {
      await axios.put("https://academic-ace-backend.vercel.app/students", {
        ...data,
        classes: classesToSet,
      });
      toast({
        title: "Estudante atualizado com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao atualizar estudante",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    }
  }

  async function deleteStudent() {
    try {
      await axios.delete(
        `https://academic-ace-backend.vercel.app/students/${id}`
      );
      toast({
        title: "Estudante removido com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao remover estudante",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    }
  }

  async function fetchCourses() {
    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/courses/list",
        {
          params: { page: 1, limit: 100 },
        }
      );

      setCourses(response.data.courses);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  async function loadClasses() {
    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/classes/list",
        {
          params: { page: 1, limit: 100 },
        }
      );

      setClassesSelected(() => {
        if (classeIds instanceof Array) {
          return classeIds.reduce((acc, cla) => {
            const c = response.data.classes.find(
              (s: { name: string; id: string }) => s.id === cla
            );

            if (c) {
              acc.push({
                label: c.name,
                value: c.name,
              });
            }

            return acc;
          }, [] as Option[]);
        }

        return [];
      });
      setClasses(response.data.classes);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchCourses();
    loadClasses();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}
      <Center width="100%" marginTop={10}>
        <Grid
          height="100%"
          width={{
            base: "100%",
            sm: "100%",
            md: "100%",
            lg: "100%",
            xl: "75%",
          }}
          templateRows="repeat(1, 1fr)"
          templateColumns="repeat(1, 1fr)"
          gap={4}
        >
          <GridItem
            colSpan={{ base: 2, sm: 2, md: 2, lg: 2, xl: 1 }}
            marginLeft={10}
            marginRight={10}
          >
            <Flex
              width="100%"
              height="100%"
              alignItems="center"
              flexDirection="column"
            >
              <Flex
                alignItems="center"
                flexDirection="column"
                justifyContent="space-arroud"
              >
                <LinkBox>
                  <Avatar
                    name=""
                    src="#"
                    showBorder={true}
                    border={`2px solid ${theme.colors.gray700}`}
                    boxSize={{ base: "50", sm: "100", md: "125", xl: "150" }}
                  />
                </LinkBox>
              </Flex>
              <Flex width="100%">
                <FormControl>
                  <FormLabel color="white">Nome</FormLabel>
                  <Controller
                    name="name"
                    control={control}
                    render={({ field }) => {
                      return (
                        <>
                          <Input
                            placeholder="Nome..."
                            color="white"
                            marginBottom="3"
                            value={field.value}
                            {...register("name")}
                          />
                          {errors.name?.message && (
                            <FormHelperText color="red">
                              {errors.name.message}
                            </FormHelperText>
                          )}
                        </>
                      );
                    }}
                  />

                  <FormLabel color="white">Email</FormLabel>
                  <Controller
                    name="email"
                    control={control}
                    render={({ field }) => {
                      return (
                        <>
                          <Input
                            placeholder="Email..."
                            color="white"
                            marginBottom="3"
                            value={field.value}
                            {...register("email")}
                          />
                          {errors.email?.message && (
                            <FormHelperText color="red">
                              {errors.email.message}
                            </FormHelperText>
                          )}
                        </>
                      );
                    }}
                  />

                  <FormLabel color="white">Curso</FormLabel>
                  <Controller
                    name="course_id"
                    control={control}
                    render={({ field }) => {
                      return (
                        <>
                          <Select
                            placeholder="Curso..."
                            color="white"
                            marginBottom="3"
                            value={field.value}
                            css={{
                              color: "var(--chakra-colors-gray-500) !important",
                            }}
                            {...register("course_id")}
                          >
                            {isLoading ? (
                              <></>
                            ) : (
                              courses &&
                              courses.map((option) => (
                                <option key={option.id} value={option.id}>
                                  {option.name}
                                </option>
                              ))
                            )}
                          </Select>
                          {errors.course_id?.message && (
                            <FormHelperText color="red">
                              {errors.course_id.message}
                            </FormHelperText>
                          )}
                        </>
                      );
                    }}
                  />

                  <FormLabel color="white">Ativo</FormLabel>
                  <Controller
                    name="active"
                    control={control}
                    render={({ field }) => {
                      return (
                        <>
                          <Select
                            color="white"
                            value={field.value}
                            css={{
                              color: "var(--chakra-colors-gray-500) !important",
                            }}
                            {...register("active")}
                          >
                            <option value="true">Ativo</option>
                            <option value="false">Desativo</option>
                          </Select>
                          {errors.active?.message && (
                            <FormHelperText color="red">
                              {errors.active.message}
                            </FormHelperText>
                          )}
                        </>
                      );
                    }}
                  />

                  <FormLabel color="white" mt={2}>
                    Turmas
                  </FormLabel>
                  <MultiSelect
                    value={classesSelected}
                    options={classes.map((classe) => {
                      return {
                        label: classe.name,
                        value: classe.name,
                      };
                    })}
                    onChange={(option) => {
                      // @ts-ignore
                      setClassesSelected(option);
                    }}
                    placeholder="Selecione as classes do aluno..."
                  />
                </FormControl>
              </Flex>
            </Flex>
          </GridItem>
        </Grid>
      </Center>
      <Flex
        width="100%"
        padding={4}
        direction="row"
        justifyContent="space-between"
        position="fixed"
        bottom={0}
        id="page-footer"
      >
        <Flex width={32} marginRight={1}>
          <BackButton></BackButton>
        </Flex>

        <Flex width={72} justifyContent="space-between">
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={`${theme.colors.red700}`}
            _hover={{
              backgroundColor: `${theme.colors.red900}`,
              color: "gray600",
            }}
            isLoading={isSubmitting}
            onClick={handleSubmit(deleteStudent)}
          >
            Deletar
          </Button>
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={"blue.500"}
            _hover={{
              backgroundColor: `blue.700`,
              color: "gray600",
            }}
            isLoading={isSubmitting}
            onClick={handleSubmit(updateStudent)}
          >
            Atualizar
          </Button>
        </Flex>
      </Flex>
    </>
  );
};

export default StudentsDetail;
