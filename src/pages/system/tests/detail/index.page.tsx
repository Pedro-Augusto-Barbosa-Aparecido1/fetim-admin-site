import {
  Text,
  Flex,
  Input,
  Button,
  Center,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
  Select,
} from "@chakra-ui/react";
import type { NextPage } from "next";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import axios, { AxiosError } from "axios";
import { theme } from "../../../../styles";
import { Controller, useForm } from "react-hook-form";
import { BackButton } from "../../../../components/General/back_button";
import { ShieldSlash } from "phosphor-react";
import { useRouter } from "next/router";
import { Loading } from "../../../../components/General/loading";
import { useState, useEffect } from "react";

interface RouterQuery {
  id: string;
  name: string;
  date: string;
  classId: string;
}

const testDataSchema = z.object({
  id: z.string(),
  name: z.string(),
  date: z.string().nonempty({ message: "Data não pode ser vazia" }),
  classId: z.string(),
});

type TestData = z.infer<typeof testDataSchema>;

const classDataSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    number: z.string(),
    teacher: z.object({ id: z.string(), name: z.string() }),
    Subject: z.object({
      id: z.string(),
      name: z.string(),
      abbreviation: z.string(),
    }),
    Student: z.array(z.object({ id: z.string(), name: z.string() })),
  })
);

type ClassData = z.infer<typeof classDataSchema>;

const TestDetail: NextPage = () => {
  const toast = useToast();
  const router = useRouter();
  const [isLoading, setLoading] = useState(false);
  const [classes, setClasses] = useState<ClassData>();

  const { id, name, date, classId } = router.query as unknown as RouterQuery;

  const {
    register,
    control,
    getValues,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<TestData>({
    resolver: zodResolver(testDataSchema),
    defaultValues: {
      id,
      name,
      date: date ? formatDateString(date) : "",
      classId,
    },
  });

  const redirect = () => {
    router.push("/system/tests");
  };

  function formatDateString(dateString: string) {
    const dateObject = new Date(dateString);
    const [year, month, day] = [
      dateObject.getFullYear(),
      (dateObject.getMonth() + 1).toString().padStart(2, "0"),
      dateObject.getMonth().toString().padStart(2, "0"),
    ];
    const formattedDate = `${year}-${month}-${day}`;
    return formattedDate;
  }

  function formatDateStringToUpdate(dateString: string) {
    const [year, month, day] = dateString.toString().split("-");
    const formattedDate = `${month}-${day}-${year}`;
    return formattedDate;
  }

  async function updateTest() {
    const data = getValues();
    try {
      setLoading(true);
      await axios.put("https://academic-ace-backend.vercel.app/tests/update/", {
        ...data,
        date: formatDateStringToUpdate(data.date),
      });
      toast({
        title: "Prova atualizada com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao atualizar Prova",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  async function deleteTest() {
    try {
      setLoading(true);
      await axios.delete(
        `https://academic-ace-backend.vercel.app/tests/delete/${id}`,
        {
          params: { id },
        }
      );
      toast({
        title: "Prova removida com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao remover Prova",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  async function fetchClasses() {
    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/classes/list",
        {
          params: { page: 1, limit: 100 },
        }
      );

      setClasses(response.data.classes);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchClasses();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}
      <Center width="100%" marginTop={10}>
        <Flex
          width="50%"
          height="100%"
          alignItems="center"
          flexDirection="column"
          margin={4}
        >
          <Text color="white" fontSize="4xl" fontWeight="bold">
            Atualizar Prova
          </Text>
          <Flex width="100%">
            <FormControl>
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Matéria..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message && (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Data</FormLabel>
              <Controller
                name="date"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        color="white"
                        type="date"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("date")}
                      />
                      {errors.date?.message ? (
                        <FormHelperText color="red" marginBottom="3">
                          {errors.date.message}
                        </FormHelperText>
                      ) : (
                        <FormHelperText marginBottom="3" />
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Turma</FormLabel>
              <Controller
                name="classId"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        placeholder="Matéria..."
                        color="white"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("classId")}
                      >
                        {isLoading ? (
                          <></>
                        ) : (
                          classes &&
                          classes.map((option) => (
                            <option key={option.id} value={option.id}>
                              {option.name}
                            </option>
                          ))
                        )}
                      </Select>
                    </>
                  );
                }}
              />
            </FormControl>
          </Flex>
        </Flex>
      </Center>
      <Flex
        width="100%"
        padding={4}
        direction="row"
        justifyContent="space-between"
        position="fixed"
        bottom={0}
        id="page-footer"
      >
        <Flex width={32}>
          <BackButton></BackButton>
        </Flex>

        <Flex width={72} justifyContent="space-between">
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={`${theme.colors.red700}`}
            _hover={{
              backgroundColor: `${theme.colors.red900}`,
              color: "gray600",
            }}
            isLoading={isSubmitting || isLoading}
            onClick={deleteTest}
          >
            Deletar
          </Button>
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={"blue.500"}
            _hover={{
              backgroundColor: `blue.700`,
              color: "gray600",
            }}
            isLoading={isSubmitting || isLoading}
            onClick={handleSubmit(updateTest)}
          >
            Atualizar
          </Button>
        </Flex>
      </Flex>
    </>
  );
};

export default TestDetail;
