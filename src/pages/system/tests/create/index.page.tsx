import {
  Text,
  Flex,
  Input,
  Button,
  Center,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
  Stack,
  Select,
} from "@chakra-ui/react";
import type { NextPage } from "next";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import axios, { AxiosError } from "axios";
import { theme } from "../../../../styles";
import { Controller, useForm } from "react-hook-form";
import { ShieldSlash } from "phosphor-react";
import { useRouter } from "next/router";
import { Loading } from "../../../../components/General/loading";
import { CancelButton } from "../../../../components/General/cancel_button";
import { useEffect, useState } from "react";

const testDataSchema = z.object({
  id: z.string(),
  name: z.string(),
  date: z.string().nonempty({ message: "Data não pode ser vazia" }),
  classId: z.string(),
});

type TestData = z.infer<typeof testDataSchema>;

const classDataSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    number: z.string(),
    teacher: z.object({ id: z.string(), name: z.string() }),
    Subject: z.object({
      id: z.string(),
      name: z.string(),
      abbreviation: z.string(),
    }),
    Student: z.array(z.object({ id: z.string(), name: z.string() })),
  })
);

type ClassData = z.infer<typeof classDataSchema>;

const TestCreate: NextPage = () => {
  const toast = useToast();
  const router = useRouter();
  const [isLoading, setLoading] = useState(false);
  const [classes, setClasses] = useState<ClassData>();

  const {
    register,
    control,
    getValues,
    formState: { isSubmitting, errors },
  } = useForm<TestData>({
    resolver: zodResolver(testDataSchema),
  });

  const redirect = () => {
    router.push("/system/tests");
  };

  async function createTest() {
    const data = getValues();

    try {
      setLoading(true);
      await axios.post(
        "https://academic-ace-backend.vercel.app/tests/create",
        data
      );

      toast({
        title: "Prova criada com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;
      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao criar prova",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  async function fetchClasses() {
    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/classes/list",
        {
          params: { page: 1, limit: 100 },
        }
      );

      setClasses(response.data.classes);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchClasses();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}
      <Center width="100%" marginTop={10}>
        <Flex
          width="50%"
          height="100%"
          alignItems="center"
          flexDirection="column"
          margin={4}
        >
          <Text color="white" fontSize="4xl" fontWeight="bold">
            Criar Prova
          </Text>
          <Flex width="100%">
            <FormControl>
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Matéria..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message && (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Data</FormLabel>
              <Controller
                name="date"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        color="white"
                        type="date"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("date")}
                      />
                      {errors.date?.message ? (
                        <FormHelperText color="red" marginBottom="3">
                          {errors.date.message}
                        </FormHelperText>
                      ) : (
                        <FormHelperText marginBottom="3" />
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Turma</FormLabel>
              <Controller
                name="classId"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        placeholder="Matéria..."
                        color="white"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("classId")}
                      >
                        {isLoading ? (
                          <></>
                        ) : (
                          classes &&
                          classes.map((option) => (
                            <option key={option.id} value={option.id}>
                              {option.name}
                            </option>
                          ))
                        )}
                      </Select>
                      {errors.classId?.message && (
                        <FormHelperText color="red">
                          {errors.classId.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <Stack direction="row" marginTop={5}>
                <CancelButton />
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  isLoading={isSubmitting || isLoading}
                  onClick={createTest}
                >
                  Cadastrar
                </Button>
              </Stack>
            </FormControl>
          </Flex>
        </Flex>
      </Center>
    </>
  );
};

export default TestCreate;
