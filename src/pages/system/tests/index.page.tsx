import {
  Flex,
  Text,
  Stack,
  Input,
  Button,
  Avatar,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";

import axios from "axios";
import type { NextPage } from "next";
import { Controller, useForm } from "react-hook-form";
import { useEffect, useState } from "react";

import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import { theme } from "../../../styles";
import { ShieldSlash } from "phosphor-react";
import { SearchIcon } from "@chakra-ui/icons";
import { TestInfo } from "../../../components/TestsInfo";
import { BackButton } from "../../../components/General/back_button";
import { CreateButton } from "../../../components/General/create_button";
import { Loading } from "../../../components/General/loading";

const testsFilterSchema = z.object({
  name: z.string(),
  start_date: z.string(),
  end_date: z.string(),
  limit: z.string(),
  page: z.string(),
});

type TestsFilterData = z.infer<typeof testsFilterSchema>;

const testsDataSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    date: z.date(),
    grade: z.string().optional(),
    classId: z.string().optional(),
    Class: z.object({ id: z.string(), name: z.string() }),
  })
);

type TestsData = z.infer<typeof testsDataSchema>;

const TestsList: NextPage = () => {
  const [tests, setTests] = useState<TestsData>();
  const [isLoading, setLoading] = useState(true);
  const toast = useToast();

  const {
    register,
    control,
    getValues,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<TestsFilterData>({
    resolver: zodResolver(testsFilterSchema),
    defaultValues: {
      name: "",
      start_date: "",
      end_date: "",
      page: "1",
      limit: "10",
    },
  });

  async function fetchTests() {
    const filters = getValues();

    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/tests/list",
        {
          params: filters,
        }
      );

      setTests(response.data.tests);
    } catch (error) {
      toast({
        title: "Erro ao listar provas",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchTests();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}

      {/* ------ Filters ------ */}
      <Flex marginLeft="10" marginTop="10" marginRight="10">
        <FormControl>
          <Stack
            direction={{
              base: "column",
              sm: "column",
              md: "column",
              lg: "row",
            }}
          >
            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "75%" }}
            >
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Nome..."
                        color="white"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message && (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </Stack>
            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "50%" }}
            >
              <FormLabel color="white">Start Date</FormLabel>
              <Controller
                name="start_date"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        color="white"
                        type="date"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("start_date")}
                      />
                      {errors.start_date?.message && (
                        <FormHelperText color="red">
                          {errors.start_date.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </Stack>
            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "50%" }}
            >
              <FormLabel color="white">End Date</FormLabel>
              <Controller
                name="end_date"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        color="white"
                        type="date"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("end_date")}
                      />
                      {errors.end_date?.message && (
                        <FormHelperText color="red">
                          {errors.end_date.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </Stack>

            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "50%" }}
              mt={{ base: "5", sm: "5", md: "5", lg: "0" }}
              mb={{ base: "5", sm: "5", md: "5", lg: "0" }}
            >
              <FormLabel
                opacity="0"
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              >
                Ignore this
              </FormLabel>
              <Flex alignItems="flex-end">
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  leftIcon={<SearchIcon />}
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  isLoading={isSubmitting}
                  loadingText=""
                  onClick={handleSubmit(fetchTests)}
                >
                  Search
                </Button>
                <CreateButton path="tests/create" />
              </Flex>
              <FormHelperText
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              />
            </Stack>
          </Stack>
        </FormControl>
      </Flex>

      <Stack
        h="30px"
        marginTop="10"
        paddingLeft="3"
        direction="row"
        marginLeft="10"
        marginRight="10"
        paddingRight="3"
        borderRadius="10"
        alignItems="center"
        color={`${theme.colors.green500}`}
      >
        <Avatar
          zIndex="-100"
          icon={<></>}
          variant="ghost"
          backgroundColor={`${theme.colors.gray800}`}
        />
        <Text fontSize="lg" width="40%" textAlign="center" fontWeight="bold">
          Nome
        </Text>
        <Text fontSize="lg" width="40%" textAlign="center" fontWeight="bold">
          Turma
        </Text>
        <Text
          fontSize="lg"
          width="40%"
          textAlign="center"
          fontWeight="bold"
          whiteSpace="nowrap"
        >
          Data
        </Text>
      </Stack>
      <Stack marginLeft="10" marginRight="10" spacing="3" zIndex="1">
        {isSubmitting ? (
          <></>
        ) : (
          tests &&
          tests.map((test, index) => (
            <TestInfo
              id={test.id}
              name={test.name}
              classData={test.Class}
              date={test.date}
              key={test.id}
            />
          ))
        )}
      </Stack>
      <Stack width="32" margin={4} position="fixed" bottom={0} id="page-footer">
        <BackButton />
      </Stack>
    </>
  );
};

export default TestsList;
