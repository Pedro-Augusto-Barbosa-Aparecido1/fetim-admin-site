import {
  Flex,
  Input,
  Stack,
  Button,
  Center,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";

import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import type { NextPage } from "next";
import axios, { AxiosError } from "axios";

import { useRouter } from "next/router";

import { ShieldSlash } from "phosphor-react";
import { Controller, useForm } from "react-hook-form";

import { theme } from "../../../../styles";
import { CancelButton } from "../../../../components/General/cancel_button";

const coursesformSchema = z.object({
  name: z.string(),
});

type CoursesFormData = z.infer<typeof coursesformSchema>;

const StudentsCreate: NextPage = () => {
  const toast = useToast();
  const router = useRouter();

  const {
    register,
    control,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<CoursesFormData>({
    resolver: zodResolver(coursesformSchema),
    defaultValues: {
      name: "",
    },
  });

  const redirect = () => {
    router.push("/system/courses");
  };

  async function createCourse(data: CoursesFormData) {
    try {
      await axios.post(
        "https://academic-ace-backend.vercel.app/courses/create",
        data
      );

      toast({
        title: "Curso criado com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;
      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao criar curso",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    }
  }

  return (
    <>
      <Center width="100%" height="100%">
        <Flex width="xl" flexDirection="column" marginLeft="5" marginRight="5">
          <Flex alignItems="center">
            <FormControl>
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Nome..."
                        color="white"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message ? (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      ) : (
                        <FormHelperText />
                      )}
                    </>
                  );
                }}
              />

              <Stack direction="row" marginTop={5}>
                <CancelButton />
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  isLoading={isSubmitting}
                  onClick={handleSubmit(createCourse)}
                >
                  Cadastrar
                </Button>
              </Stack>
            </FormControl>
          </Flex>
        </Flex>
      </Center>
    </>
  );
};

export default StudentsCreate;
