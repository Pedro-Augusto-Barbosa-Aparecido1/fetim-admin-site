import {
  Flex,
  Stack,
  Input,
  Button,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";

import axios from "axios";
import type { NextPage } from "next";
import { Controller, useForm } from "react-hook-form";
import { useEffect, useState } from "react";

import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import { theme } from "../../../styles";
import { ShieldSlash } from "phosphor-react";
import { SearchIcon } from "@chakra-ui/icons";
import { CourseInfo } from "../../../components/CourseInfo";
import { BackButton } from "../../../components/General/back_button";
import { CreateButton } from "../../../components/General/create_button";
import { Loading } from "../../../components/General/loading";

const coursesFilterSchema = z.object({
  name: z.string(),
  page: z.string(),
  limit: z.string(),
});

type CoursesFilterData = z.infer<typeof coursesFilterSchema>;

const coursesGetSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    page: z.string(),
    total: z.string(),
  })
);

type CoursesGetData = z.infer<typeof coursesGetSchema>;

const Courses: NextPage = () => {
  const [courses, setCourses] = useState<CoursesGetData>();
  const [isLoading, setLoading] = useState(true);
  const toast = useToast();

  const {
    register,
    control,
    getValues,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<CoursesFilterData>({
    resolver: zodResolver(coursesFilterSchema),
    defaultValues: {
      name: "",
      page: "1",
      limit: "10",
    },
  });

  async function fetchCourses() {
    const filters = getValues();

    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/courses/list",
        {
          params: filters,
        }
      );

      setCourses(response.data.courses);
    } catch (error) {
      console.log(error);
      toast({
        title: "Erro ao listar cursos",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchCourses();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}

      {/* ------ Filters ------ */}
      <Flex marginLeft="10" marginTop="10" marginRight="10">
        <FormControl>
          <Stack
            direction={{
              base: "column",
              sm: "column",
              md: "column",
              lg: "row",
            }}
          >
            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "100%" }}
              marginRight={{ base: "0", sm: "0", md: "0", lg: "35%" }}
            >
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Nome..."
                        color="white"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message && (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </Stack>

            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "25%" }}
              mt={{ base: "5", sm: "5", md: "5", lg: "0" }}
              mb={{ base: "5", sm: "5", md: "5", lg: "0" }}
            >
              <FormLabel
                opacity="0"
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              >
                Ignore this
              </FormLabel>
              <Flex alignItems="flex-end">
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  leftIcon={<SearchIcon />}
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  isLoading={isSubmitting}
                  loadingText=""
                  onClick={handleSubmit(fetchCourses)}
                >
                  Search
                </Button>
                <CreateButton path="courses/create" />
              </Flex>
              <FormHelperText
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              />
            </Stack>
          </Stack>
        </FormControl>
      </Flex>

      {/* ------ Course List ------ */}
      <Stack
        marginLeft="10"
        marginRight="10"
        marginTop="5"
        spacing="3"
        zIndex="1"
      >
        {isSubmitting ? (
          <></>
        ) : (
          courses &&
          courses.map((course, index) => (
            <CourseInfo id={course.id} name={course.name} key={index} />
          ))
        )}
      </Stack>
      <Stack width="32" margin={4} position="fixed" bottom={0} id="page-footer">
        <BackButton />
      </Stack>
    </>
  );
};

export default Courses;
