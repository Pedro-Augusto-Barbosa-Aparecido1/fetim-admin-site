import {
  Flex,
  Text,
  Input,
  Button,
  Center,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";
import type { NextPage } from "next";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import axios, { AxiosError } from "axios";
import { theme } from "../../../../styles";
import { Controller, useForm } from "react-hook-form";
import { BackButton } from "../../../../components/General/back_button";
import { ShieldSlash } from "phosphor-react";
import { useRouter } from "next/router";
import { Loading } from "../../../../components/General/loading";

const coursesformSchema = z.object({
  id: z.string(),
  name: z.string(),
});

type CoursesFormData = z.infer<typeof coursesformSchema>;

const CourseDetail: NextPage = () => {
  const toast = useToast();
  const router = useRouter();

  let { id, name } = router.query;

  // Ensure that the values are string
  id = id ? (Array.isArray(id) ? id[0] : id) : "";
  name = name ? (Array.isArray(name) ? name[0] : name) : "";

  const {
    register,
    control,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<CoursesFormData>({
    resolver: zodResolver(coursesformSchema),
    defaultValues: {
      id,
      name,
    },
  });

  const redirect = () => {
    router.push("/system/courses");
  };

  async function updateCourse(data: CoursesFormData) {
    try {
      await axios.put(
        "https://academic-ace-backend.vercel.app/courses/update/",
        data
      );
      toast({
        title: "Curso atualizado com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao atualizar curso",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    }
  }

  async function deleteCourse() {
    try {
      await axios.delete(
        `https://academic-ace-backend.vercel.app/courses/?id=${id}`
      );
      toast({
        title: "Curso removido com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao remover curso",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    }
  }

  return (
    <>
      {isSubmitting ? <Loading /> : <></>}
      <Center width="100%" marginTop={10}>
        <Flex
          width="50%"
          height="100%"
          alignItems="center"
          flexDirection="column"
          margin={4}
        >
          <Text color="white" fontSize="4xl" fontWeight="bold">
            Atualizar Curso
          </Text>
          <Flex width="100%">
            <FormControl>
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Nome..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message && (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </FormControl>
          </Flex>
        </Flex>
      </Center>
      <Flex
        width="100%"
        padding={4}
        direction="row"
        justifyContent="space-between"
        position="fixed"
        bottom={0}
        id="page-footer"
      >
        <Flex width={32}>
          <BackButton></BackButton>
        </Flex>

        <Flex width={72} justifyContent="space-between">
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={`${theme.colors.red700}`}
            _hover={{
              backgroundColor: `${theme.colors.red900}`,
              color: "gray600",
            }}
            isLoading={isSubmitting}
            onClick={handleSubmit(deleteCourse)}
          >
            Deletar
          </Button>
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={"blue.500"}
            _hover={{
              backgroundColor: `blue.700`,
              color: "gray600",
            }}
            isLoading={isSubmitting}
            onClick={handleSubmit(updateCourse)}
          >
            Atualizar
          </Button>
        </Flex>
      </Flex>
    </>
  );
};

export default CourseDetail;
