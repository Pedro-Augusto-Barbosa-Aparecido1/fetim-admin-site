import {
  Text,
  Flex,
  Input,
  Button,
  Center,
  Select,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
  Stack,
} from "@chakra-ui/react";
import type { NextPage } from "next";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import axios, { AxiosError } from "axios";
import { theme } from "../../../../styles";
import { Controller, useForm } from "react-hook-form";
import { ShieldSlash } from "phosphor-react";
import { useRouter } from "next/router";
import { Loading } from "../../../../components/General/loading";
import { useEffect, useState } from "react";
import { CancelButton } from "../../../../components/General/cancel_button";

const classDataSchema = z.object({
  id: z.string(),
  name: z.string(),
  number: z.string(),
  teacher_id: z.string(),
  subject_id: z.string(),
  Student: z.array(z.object({ id: z.string(), name: z.string() })),
});

type ClassData = z.infer<typeof classDataSchema>;

const teacherFilterSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    role: z.string(),
    page: z.string(),
    limit: z.string(),
  })
);

type TeacherFilterData = z.infer<typeof teacherFilterSchema>;

const subjectFilterSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    abbreviation: z.string(),
    page: z.string(),
    limit: z.string(),
  })
);

type SubjectFilterData = z.infer<typeof subjectFilterSchema>;

const ClassCreate: NextPage = () => {
  const toast = useToast();
  const router = useRouter();
  const [teachers, setTeachers] = useState<TeacherFilterData>();
  const [subjects, setSubjects] = useState<SubjectFilterData>();
  const [isLoading, setLoading] = useState(true);

  const {
    register,
    control,
    getValues,
    formState: { isSubmitting, errors },
  } = useForm<ClassData>({
    resolver: zodResolver(classDataSchema),
  });

  const redirect = () => {
    router.push("/system/classes");
  };

  async function fetchTeachers() {
    try {
      setLoading(true);

      const response = await axios.post(
        "https://academic-ace-backend.vercel.app/users/list",
        {
          role: "teacher",
          page: 0,
          limit: 100,
        }
      );

      setTeachers(response.data.users);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  async function fetchSubjects() {
    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/subjects/list",
        {
          params: { page: 1, limit: 100 },
        }
      );

      setSubjects(response.data.subjects);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  async function createClass() {
    const data = getValues();

    try {
      setLoading(true);
      await axios.post(
        "https://academic-ace-backend.vercel.app/classes/create",
        data
      );

      toast({
        title: "Turma criada com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;
      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao criar turma",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchTeachers();
    fetchSubjects();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}
      <Center width="100%" marginTop={10}>
        <Flex
          width="50%"
          height="100%"
          alignItems="center"
          flexDirection="column"
          margin={4}
        >
          <Text color="white" fontSize="4xl" fontWeight="bold">
            Criar Turma
          </Text>
          <Flex width="100%">
            <FormControl>
              <FormLabel color="white">Turma</FormLabel>
              <Controller
                name="number"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Turma..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        {...register("number")}
                      />
                      {errors.number?.message && (
                        <FormHelperText color="red">
                          {errors.number.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Matéria</FormLabel>
              <Controller
                name="subject_id"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        placeholder="Matéria..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("subject_id")}
                      >
                        {isLoading ? (
                          <></>
                        ) : (
                          subjects &&
                          subjects.map((option) => (
                            <option key={option.id} value={option.id}>
                              {option.abbreviation + " - " + option.name}
                            </option>
                          ))
                        )}
                      </Select>
                      {errors.subject_id?.message && (
                        <FormHelperText color="red">
                          {errors.subject_id.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Professor</FormLabel>
              <Controller
                name="teacher_id"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        placeholder="Professor..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("teacher_id")}
                      >
                        {isLoading ? (
                          <></>
                        ) : (
                          teachers &&
                          teachers.map((option) => (
                            <option key={option.id} value={option.id}>
                              {option.name}
                            </option>
                          ))
                        )}
                      </Select>
                      {errors.teacher_id?.message && (
                        <FormHelperText color="red">
                          {errors.teacher_id.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <Stack direction="row" marginTop={5}>
                <CancelButton />
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  isLoading={isSubmitting || isLoading}
                  onClick={createClass}
                >
                  Cadastrar
                </Button>
              </Stack>
            </FormControl>
          </Flex>
        </Flex>
      </Center>
    </>
  );
};

export default ClassCreate;
