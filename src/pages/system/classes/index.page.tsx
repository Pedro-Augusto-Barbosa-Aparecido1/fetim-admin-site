import {
  Text,
  Flex,
  Stack,
  Input,
  Button,
  Select,
  FormLabel,
  FormControl,
  Avatar,
  FormHelperText,
} from "@chakra-ui/react";
import { NextPage } from "next";
import { theme } from "../../../styles";
import { SearchIcon } from "@chakra-ui/icons";
import { z } from "zod";
import axios from "axios";
import { zodResolver } from "@hookform/resolvers/zod";
import { ClassInfo } from "../../../components/ClassInfo";
import { BackButton } from "../../../components/General/back_button";
import { CreateButton } from "../../../components/General/create_button";
import { useState, useEffect } from "react";
import { Controller, useForm } from "react-hook-form";
import { Loading } from "../../../components/General/loading";

const classDataSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    number: z.string(),
    teacher: z.object({ id: z.string(), name: z.string() }),
    Subject: z.object({
      id: z.string(),
      name: z.string(),
      abbreviation: z.string(),
    }),
    Student: z.array(z.object({ id: z.string(), name: z.string() })),
  })
);

type ClassData = z.infer<typeof classDataSchema>;

const teacherFilterSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    role: z.string(),
    page: z.string(),
    limit: z.string(),
  })
);

type TeacherFilterData = z.infer<typeof teacherFilterSchema>;

const subjectFilterSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    abbreviation: z.string(),
    page: z.string(),
    limit: z.string(),
  })
);

type SubjectFilterData = z.infer<typeof subjectFilterSchema>;

const filterSchema = z.object({
  number: z.string(),
  subject_id: z.string(),
  teacherId: z.string(),
  page: z.string(),
  limit: z.string(),
});

type FilterData = z.infer<typeof filterSchema>;

const Classes: NextPage = () => {
  const [classes, setClasses] = useState<ClassData>();
  const [teachers, setTeachers] = useState<TeacherFilterData>();
  const [subjects, setSubjects] = useState<SubjectFilterData>();
  const [isLoading, setLoading] = useState(true);

  const {
    register,
    control,
    getValues,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<FilterData>({
    resolver: zodResolver(filterSchema),
    defaultValues: {
      number: "",
      subject_id: "",
      teacherId: "",
      page: "1",
      limit: "100",
    },
  });

  async function fetchTeachers() {
    try {
      setLoading(true);

      const response = await axios.post(
        "https://academic-ace-backend.vercel.app/users/list",
        {
          role: "teacher",
          page: 1,
          limit: 100,
        }
      );

      setTeachers(response.data.users);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  async function fetchSubjects() {
    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/subjects/list",
        {
          params: { page: 1, limit: 100 },
        }
      );

      setSubjects(response.data.subjects);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  async function fetchClasses() {
    const filters = getValues();

    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/classes/list",
        {
          params: { ...filters },
        }
      );

      setClasses(response.data.classes);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchTeachers();
    fetchSubjects();
    fetchClasses();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isSubmitting || isLoading ? <Loading /> : <></>}
      <Flex marginLeft="10" marginTop="10" marginRight="10">
        <FormControl>
          <Stack
            direction={{
              base: "column",
              sm: "column",
              md: "row",
              lg: "row",
            }}
          >
            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "75%", lg: "75%" }}
            >
              <FormLabel color="white">Turma</FormLabel>
              <Controller
                name="number"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Turma..."
                        color="white"
                        value={field.value}
                        {...register("number")}
                      />
                      {errors.number?.message ? (
                        <FormHelperText color="red">
                          {errors.number.message}
                        </FormHelperText>
                      ) : (
                        <FormHelperText />
                      )}
                    </>
                  );
                }}
              ></Controller>
            </Stack>
            <Stack spacing="0" width="100%">
              <FormLabel color="white">Matéria</FormLabel>
              <Controller
                name="subject_id"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        placeholder="Matéria..."
                        color="white"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("subject_id")}
                      >
                        {isLoading ? (
                          <></>
                        ) : (
                          subjects &&
                          subjects.map((option) => (
                            <option key={option.id} value={option.id}>
                              {option.abbreviation + " - " + option.name}
                            </option>
                          ))
                        )}
                      </Select>
                    </>
                  );
                }}
              ></Controller>
            </Stack>
            <Stack spacing="0" width="100%">
              <FormLabel color="white">Professor</FormLabel>
              <Controller
                name="teacherId"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        placeholder="Professor..."
                        color="white"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("teacherId")}
                      >
                        {isLoading ? (
                          <></>
                        ) : (
                          teachers &&
                          teachers.map((option) => (
                            <option key={option.id} value={option.id}>
                              {option.name}
                            </option>
                          ))
                        )}
                      </Select>
                    </>
                  );
                }}
              />
            </Stack>

            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "25%" }}
              mt={{ base: "5", sm: "5", md: "5", lg: "0" }}
              mb={{ base: "5", sm: "5", md: "5", lg: "0" }}
            >
              <FormLabel
                opacity="0"
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              >
                Ignore this
              </FormLabel>
              <Flex alignItems="flex-end">
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  leftIcon={<SearchIcon />}
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  onClick={handleSubmit(fetchClasses)}
                >
                  Search
                </Button>
                <CreateButton path="classes/create" />
              </Flex>
              <FormHelperText
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              />
            </Stack>
          </Stack>
        </FormControl>
      </Flex>
      <Stack
        h="30px"
        marginTop="10"
        paddingLeft="3"
        direction="row"
        marginLeft="10"
        marginRight="10"
        paddingRight="3"
        borderRadius="10"
        alignItems="center"
        color={`${theme.colors.green500}`}
      >
        <Avatar
          zIndex="-100"
          icon={<></>}
          variant="ghost"
          backgroundColor={`${theme.colors.gray800}`}
        />
        <Text fontSize="lg" width="40%" textAlign="center" fontWeight="bold">
          Turma
        </Text>
        <Text fontSize="lg" width="40%" textAlign="center" fontWeight="bold">
          Matéria
        </Text>
        <Text fontSize="lg" width="100%" textAlign="center" fontWeight="bold">
          Professor
        </Text>
        <Text
          fontSize="lg"
          width="40%"
          textAlign="center"
          fontWeight="bold"
          whiteSpace="nowrap"
        >
          Qtd. Alunos
        </Text>
      </Stack>
      <Stack marginLeft="10" marginRight="10" spacing="3" zIndex="1">
        {isSubmitting ? (
          <></>
        ) : (
          classes &&
          classes.map((classData, index) => (
            <ClassInfo
              id={classData.id}
              key={classData.id}
              name={classData.name}
              number={classData.number}
              subject={classData.Subject}
              teacher={classData.teacher}
              student={classData.Student}
            />
          ))
        )}
      </Stack>
      <Stack width="32" margin={4} position="fixed" bottom={0} id="page-footer">
        <BackButton />
      </Stack>
    </>
  );
};

export default Classes;
