import {
  Text,
  Flex,
  Input,
  Button,
  Center,
  Select,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";
import type { NextPage } from "next";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import axios, { AxiosError } from "axios";
import { theme } from "../../../../styles";
import { Controller, useForm } from "react-hook-form";
import { BackButton } from "../../../../components/General/back_button";
import { ShieldSlash } from "phosphor-react";
import { useRouter } from "next/router";
import { Loading } from "../../../../components/General/loading";
import { useEffect, useState } from "react";

const classDataSchema = z.object({
  id: z.string(),
  name: z.string(),
  number: z.string(),
  teacher: z.string(),
  Subject: z.string(),
  Student: z.array(z.object({ id: z.string(), name: z.string() })),
});

type ClassData = z.infer<typeof classDataSchema>;

interface RouterQuery {
  id?: string;
  name?: string;
  number?: string;
  teacher?: string;
  subject?: string;
  student?: Array<{ id: string; name: string }>;
  qtdStudents?: string;
}

const teacherFilterSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    role: z.string(),
    page: z.string(),
    limit: z.string(),
  })
);

type TeacherFilterData = z.infer<typeof teacherFilterSchema>;

const subjectFilterSchema = z.array(
  z.object({
    id: z.string(),
    name: z.string(),
    abbreviation: z.string(),
    page: z.string(),
    limit: z.string(),
  })
);

type SubjectFilterData = z.infer<typeof subjectFilterSchema>;

const ClassDetail: NextPage = () => {
  const toast = useToast();
  const router = useRouter();
  const [teachers, setTeachers] = useState<TeacherFilterData>();
  const [subjects, setSubjects] = useState<SubjectFilterData>();
  const [isLoading, setLoading] = useState(true);

  const { id, name, number, teacher, subject, student, qtdStudents } =
    router.query as RouterQuery;

  const {
    register,
    control,
    getValues,
    formState: { isSubmitting, errors },
  } = useForm<ClassData>({
    resolver: zodResolver(classDataSchema),
    defaultValues: {
      id,
      name,
      number,
      teacher,
      Subject: subject,
      Student: student,
    },
  });

  const redirect = () => {
    router.push("/system/classes");
  };

  async function updateClass() {
    const data = getValues();
    try {
      setLoading(true);
      await axios.put(
        "https://academic-ace-backend.vercel.app/classes/update/",
        data
      );
      toast({
        title: "Turma atualizada com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao atualizar turma",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  async function deleteClass() {
    try {
      setLoading(true);
      await axios.delete(
        `https://academic-ace-backend.vercel.app/classes/delete/${id}`,
        {
          params: { id },
        }
      );
      toast({
        title: "Turma removida com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao remover turma",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  async function fetchTeachers() {
    try {
      setLoading(true);

      const response = await axios.post(
        "https://academic-ace-backend.vercel.app/users/list",
        {
          role: "teacher",
          page: 0,
          limit: 100,
        }
      );

      setTeachers(response.data.users);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  async function fetchSubjects() {
    try {
      setLoading(true);

      const response = await axios.get(
        "https://academic-ace-backend.vercel.app/subjects/list",
        {
          params: { page: 1, limit: 100 },
        }
      );

      setSubjects(response.data.subjects);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchTeachers();
    fetchSubjects();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}
      <Center width="100%" marginTop={10}>
        <Flex
          width="50%"
          height="100%"
          alignItems="center"
          flexDirection="column"
          margin={4}
        >
          <Text color="white" fontSize="4xl" fontWeight="bold">
            Atualizar Turma
          </Text>
          <Flex width="100%">
            <FormControl>
              <FormLabel color="white">Turma</FormLabel>
              <Controller
                name="number"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Turma..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        {...register("number")}
                      />
                      {errors.number?.message && (
                        <FormHelperText color="red">
                          {errors.number.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Matéria</FormLabel>
              <Controller
                name="Subject"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        placeholder="Matéria..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("Subject")}
                      >
                        {isLoading ? (
                          <></>
                        ) : (
                          subjects &&
                          subjects.map((option) => (
                            <option key={option.id} value={option.id}>
                              {option.abbreviation + " - " + option.name}
                            </option>
                          ))
                        )}
                      </Select>
                      {errors.Subject?.message && (
                        <FormHelperText color="red">
                          {errors.Subject.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Professor</FormLabel>
              <Controller
                name="teacher"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Select
                        placeholder="Professor..."
                        color="white"
                        marginBottom="3"
                        value={field.value}
                        css={{
                          color: "var(--chakra-colors-gray-500) !important",
                        }}
                        {...register("teacher")}
                      >
                        {isLoading ? (
                          <></>
                        ) : (
                          teachers &&
                          teachers.map((option) => (
                            <option key={option.id} value={option.id}>
                              {option.name}
                            </option>
                          ))
                        )}
                      </Select>
                      {errors.teacher?.message && (
                        <FormHelperText color="red">
                          {errors.teacher.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
              <FormLabel color="white">Qtd. Alunos</FormLabel>
              <Input
                placeholder="Nome..."
                color="white"
                value={qtdStudents}
                disabled={true}
              />
            </FormControl>
          </Flex>
        </Flex>
      </Center>
      <Flex
        width="100%"
        padding={4}
        direction="row"
        justifyContent="space-between"
        position="fixed"
        bottom={0}
        id="page-footer"
      >
        <Flex width={32}>
          <BackButton></BackButton>
        </Flex>

        <Flex width={72} justifyContent="space-between">
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={`${theme.colors.red700}`}
            _hover={{
              backgroundColor: `${theme.colors.red900}`,
              color: "gray600",
            }}
            isLoading={isSubmitting || isLoading}
            onClick={deleteClass}
          >
            Deletar
          </Button>
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={"blue.500"}
            _hover={{
              backgroundColor: `blue.700`,
              color: "gray600",
            }}
            isLoading={isSubmitting || isLoading}
            onClick={updateClass}
          >
            Atualizar
          </Button>
        </Flex>
      </Flex>
    </>
  );
};

export default ClassDetail;
