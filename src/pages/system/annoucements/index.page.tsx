import {
  Text,
  Flex,
  Image,
  Input,
  Button,
  Center,
  Textarea,
  FormLabel,
  FormControl,
} from "@chakra-ui/react";

import type { NextPage } from "next";
import { theme } from "../../../styles";

const Support: NextPage = () => {
  return (
    <>
      <Center width="100%" height="100%">
        <Flex width="xl" flexDirection="column" marginLeft="5" marginRight="5">
          <Flex
            alignItems="center"
            justifyContent="space-arroud"
            flexDirection="column"
            marginBottom="25px"
          >
            <Image src="/logo.svg" alt="logo" boxSize="36" />
            <Text
              fontSize="4xl"
              color="white"
              fontWeight="bold"
              noOfLines={2}
              textAlign="center"
            >
              Novo Aviso
            </Text>
          </Flex>

          <Flex alignItems="center">
            <FormControl isRequired>
              <FormLabel color="white">Título</FormLabel>
              <Input
                placeholder="Título do aviso..."
                color="white"
                marginBottom="25px"
              />

              <FormLabel color="white">Mensagem</FormLabel>
              <Textarea
                color="white"
                resize="none"
                placeholder="Escreva a mensagem aqui..."
                marginBottom="30px"
              />

              <Button
                width="100%"
                type="submit"
                color="white"
                backgroundColor={`${theme.colors.green500}`}
                _hover={{
                  backgroundColor: `${theme.colors.green800}`,
                  color: "gray600",
                }}
              >
                Enviar
              </Button>
            </FormControl>
          </Flex>
        </Flex>
      </Center>
    </>
  );
};

export default Support;
