import {
  Flex,
  Input,
  Stack,
  Button,
  Center,
  Avatar,
  LinkBox,
  useToast,
  FormLabel,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";

import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import type { NextPage } from "next";
import { useRouter } from "next/router";
import axios, { AxiosError } from "axios";

import { ShieldSlash } from "phosphor-react";
import { Controller, useForm } from "react-hook-form";

import { theme } from "../../../../styles";
import { CancelButton } from "../../../../components/General/cancel_button";

const teachersformSchema = z.object({
  name: z.string(),
  email: z.string().email(),
  password: z.string(),
});

type TeachersFormData = z.infer<typeof teachersformSchema>;

const StudentsCreate: NextPage = () => {
  const toast = useToast();
  const router = useRouter();

  const {
    register,
    control,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<TeachersFormData>({
    resolver: zodResolver(teachersformSchema),
    defaultValues: {
      name: "",
      email: "",
      password: "AcademicAce@2023",
    },
  });

  const redirect = () => {
    router.push("/system/teachers");
  };

  async function createTeacher(data: TeachersFormData) {
    try {
      await axios.post("https://academic-ace-backend.vercel.app/users/create", {
        ...data,
        role: "teacher",
      });

      toast({
        title: "Professor criado com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;
      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao criar professor",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    }
  }

  return (
    <>
      <Center width="100%" height="100%">
        <Flex width="xl" flexDirection="column" marginLeft="5" marginRight="5">
          <Flex
            alignItems="center"
            marginBottom="25px"
            flexDirection="column"
            justifyContent="space-arroud"
          >
            <LinkBox>
              <Avatar
                name=""
                src="#"
                boxSize="150"
                cursor="pointer"
                showBorder={true}
                border={`2px solid ${theme.colors.gray700}`}
                _hover={{
                  opacity: "0.6",
                }}
              />
            </LinkBox>
          </Flex>

          <Flex alignItems="center">
            <FormControl>
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Nome..."
                        color="white"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message ? (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      ) : (
                        <FormHelperText />
                      )}
                    </>
                  );
                }}
              />

              <FormLabel color="white" marginTop="1">
                Email
              </FormLabel>
              <Controller
                name="email"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Email..."
                        color="white"
                        value={field.value}
                        {...register("email")}
                      />
                      {errors.email?.message ? (
                        <FormHelperText color="red">
                          {errors.email.message}
                        </FormHelperText>
                      ) : (
                        <FormHelperText />
                      )}
                    </>
                  );
                }}
              />

              <Stack direction="row" marginTop={5}>
                <CancelButton />
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  isLoading={isSubmitting}
                  onClick={handleSubmit(createTeacher)}
                >
                  Cadastrar
                </Button>
              </Stack>
            </FormControl>
          </Flex>
        </Flex>
      </Center>
    </>
  );
};

export default StudentsCreate;
