import {
  Flex,
  Stack,
  Input,
  Button,
  useToast,
  FormLabel,
  SimpleGrid,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";

import axios from "axios";
import type { NextPage } from "next";
import { Controller, useForm } from "react-hook-form";
import { useEffect, useState } from "react";

import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import { theme } from "../../../styles";
import { ShieldSlash } from "phosphor-react";
import { SearchIcon } from "@chakra-ui/icons";
import { UserCard } from "../../../components/UserInfo/card";
import { BackButton } from "../../../components/General/back_button";
import { CreateButton } from "../../../components/General/create_button";
import { Loading } from "../../../components/General/loading";

const teachersFilterSchema = z.object({
  name: z.string(),
  email: z
    .string()
    .refine(
      (value) =>
        value === "" || /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value),
      {
        message: "Email inválido",
      }
    ),
  role: z.string(),
  limit: z.string(),
  page: z.string(),
});

type TeachersFilterData = z.infer<typeof teachersFilterSchema>;

// const teachersformSchema = z.array(
//   z.object({
//     id: z.string(),
//     name: z.string(),
//     email: z.string().email(),
//   })
// );

type TeachersFormData = {
  id: string;
  name: string;
  email: string;
};

const Teachers: NextPage = () => {
  const [teachers, setTeachers] = useState<TeachersFormData[]>();
  const [isLoading, setLoading] = useState(true);
  const toast = useToast();

  const {
    register,
    control,
    getValues,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<TeachersFilterData>({
    resolver: zodResolver(teachersFilterSchema),
    defaultValues: {
      name: "",
      email: "",
      role: "teacher",
      page: "0",
      limit: "10",
    },
  });

  async function fetchTeachers() {
    const filters = getValues();

    try {
      setLoading(true);

      const response = await axios.post(
        "https://academic-ace-backend.vercel.app/users/list",
        filters
      );

      setTeachers(response.data.users);
    } catch (error) {
      toast({
        title: "Erro ao listar professores",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchTeachers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}

      {/* ------ Filters ------ */}
      <Flex marginLeft="10" marginTop="10" marginRight="10">
        <FormControl>
          <Stack
            direction={{
              base: "column",
              sm: "column",
              md: "column",
              lg: "row",
            }}
          >
            <Stack spacing="0" width="100%">
              <FormLabel color="white">Email</FormLabel>
              <Controller
                name="email"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Email..."
                        color="white"
                        value={field.value}
                        {...register("email")}
                      />
                      {errors.email?.message && (
                        <FormHelperText color="red">
                          {errors.email.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </Stack>
            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "75%" }}
            >
              <FormLabel color="white">Nome</FormLabel>
              <Controller
                name="name"
                control={control}
                render={({ field }) => {
                  return (
                    <>
                      <Input
                        placeholder="Nome..."
                        color="white"
                        value={field.value}
                        {...register("name")}
                      />
                      {errors.name?.message && (
                        <FormHelperText color="red">
                          {errors.name.message}
                        </FormHelperText>
                      )}
                    </>
                  );
                }}
              />
            </Stack>
            <Stack
              spacing="0"
              width={{ base: "100%", sm: "100%", md: "100%", lg: "50%" }}
              mt={{ base: "5", sm: "5", md: "5", lg: "0" }}
              mb={{ base: "5", sm: "5", md: "5", lg: "0" }}
            >
              <FormLabel
                opacity="0"
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              >
                Ignore this
              </FormLabel>
              <Flex alignItems="flex-end">
                <Button
                  width="100%"
                  type="submit"
                  color="white"
                  leftIcon={<SearchIcon />}
                  backgroundColor={`${theme.colors.green500}`}
                  _hover={{
                    backgroundColor: `${theme.colors.green800}`,
                    color: "gray600",
                  }}
                  isLoading={isSubmitting}
                  loadingText=""
                  onClick={handleSubmit(fetchTeachers)}
                >
                  Search
                </Button>
                <CreateButton path="teachers/create" />
              </Flex>
              <FormHelperText
                display={{ base: "none", sm: "none", md: "none", lg: "block" }}
              />
            </Stack>
          </Stack>
        </FormControl>
      </Flex>

      {/* ------ Student List ------ */}
      <SimpleGrid
        margin="10"
        marginTop="5"
        spacing={6}
        templateColumns="repeat(auto-fill, minmax(250px, 1fr))"
      >
        {isSubmitting ? (
          <></>
        ) : (
          teachers &&
          teachers.map((teacher, index) => (
            <UserCard
              id={teacher.id}
              img=""
              active=""
              name={teacher.name}
              email={teacher.email}
              course={{ name: "", id: "" }}
              key={teacher.id}
              pathname="teachers/detail"
            />
          ))
        )}
      </SimpleGrid>
      <Stack width="32" margin={4} position="fixed" bottom={0} id="page-footer">
        <BackButton />
      </Stack>
    </>
  );
};

export default Teachers;
