import {
  Flex,
  Grid,
  Input,
  Button,
  Center,
  Avatar,
  LinkBox,
  useToast,
  GridItem,
  FormLabel,
  FormControl,
  FormHelperText,
} from "@chakra-ui/react";
import type { NextPage } from "next";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import { useRouter } from "next/router";

import axios, { AxiosError } from "axios";
import { theme } from "../../../../styles";
import { Controller, useForm } from "react-hook-form";
import { BackButton } from "../../../../components/General/back_button";
import { ShieldSlash } from "phosphor-react";

import { useState } from "react";
import { Loading } from "../../../../components/General/loading";

const teachersFormSchema = z.object({
  id: z.string(),
  name: z.string(),
  email: z
    .string()
    .refine(
      (value) =>
        value === "" || /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value),
      {
        message: "Email inválido",
      }
    ),
});

type TeachersFormData = z.infer<typeof teachersFormSchema>;

const StudentsDetail: NextPage = () => {
  const toast = useToast();
  const router = useRouter();
  const [isLoading, setLoading] = useState(false);

  let { id, name, email } = router.query;

  // Ensure that the values are string
  id = id ? (Array.isArray(id) ? id[0] : id) : "";
  name = name ? (Array.isArray(name) ? name[0] : name) : "";
  email = email ? (Array.isArray(email) ? email[0] : email) : "";

  const {
    register,
    control,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<TeachersFormData>({
    resolver: zodResolver(teachersFormSchema),
    defaultValues: {
      id,
      name,
      email,
    },
  });

  const redirect = () => {
    router.push("/system/teachers");
  };

  async function updateStudent(data: TeachersFormData) {
    try {
      setLoading(true);
      await axios.put(
        `https://academic-ace-backend.vercel.app/users/?id=${data.id}`,
        data
      );
      toast({
        title: "Professor atualizado com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao atualizar professor",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    } finally {
      setLoading(false);
    }
  }

  async function deleteStudent() {
    try {
      await axios.delete(
        `https://academic-ace-backend.vercel.app/users/?id=${id}`
      );
      toast({
        title: "Professor removido com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        duration: 2000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        onCloseComplete: redirect,
      });
    } catch (error) {
      const apiError = error as AxiosError;

      console.log("Error:", apiError.response?.data);

      toast({
        title: "Erro ao remover professor",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 2500,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    }
  }

  return (
    <>
      {isLoading || isSubmitting ? <Loading /> : <></>}
      <Center width="100%" height="100%" marginTop={10}>
        <Grid
          height="100%"
          width="50%"
          templateRows={{
            base: "repeat(2, 1fr)",
            sm: "repeat(2, 1fr)",
            md: "repeat(2, 1fr)",
            lg: "repeat(1, 1fr)",
            xl: "repeat(1, 1fr)",
          }}
          templateColumns="repeat(1, 1fr)"
          gap={4}
          margin={4}
        >
          <GridItem
            colSpan={{ base: 2, sm: 2, md: 2, lg: 2, xl: 1 }}
            marginLeft={10}
            marginRight={10}
          >
            <Flex
              width="100%"
              height="100%"
              alignItems="center"
              flexDirection="column"
            >
              <Flex
                alignItems="center"
                flexDirection="column"
                justifyContent="space-arroud"
              >
                <LinkBox>
                  <Avatar
                    name=""
                    src="#"
                    showBorder={true}
                    border={`2px solid ${theme.colors.gray700}`}
                    boxSize={{ base: "50", sm: "100", md: "125", xl: "150" }}
                  />
                </LinkBox>
              </Flex>
              <Flex width="100%">
                <FormControl>
                  <FormLabel color="white">Nome</FormLabel>
                  <Controller
                    name="name"
                    control={control}
                    render={({ field }) => {
                      return (
                        <>
                          <Input
                            placeholder="Nome..."
                            color="white"
                            marginBottom="3"
                            value={field.value}
                            {...register("name")}
                          />
                          {errors.name?.message && (
                            <FormHelperText color="red">
                              {errors.name.message}
                            </FormHelperText>
                          )}
                        </>
                      );
                    }}
                  />

                  <FormLabel color="white">Email</FormLabel>
                  <Controller
                    name="email"
                    control={control}
                    render={({ field }) => {
                      return (
                        <>
                          <Input
                            placeholder="Email..."
                            color="white"
                            marginBottom="3"
                            value={field.value}
                            {...register("email")}
                          />
                          {errors.email?.message && (
                            <FormHelperText color="red">
                              {errors.email.message}
                            </FormHelperText>
                          )}
                        </>
                      );
                    }}
                  />
                </FormControl>
              </Flex>
            </Flex>
          </GridItem>
        </Grid>
      </Center>
      <Flex
        width="100%"
        padding={4}
        direction="row"
        justifyContent="space-between"
        position="fixed"
        bottom={0}
        id="page-footer"
      >
        <Flex width={32}>
          <BackButton></BackButton>
        </Flex>

        <Flex width={72} justifyContent="space-between">
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={`${theme.colors.red700}`}
            _hover={{
              backgroundColor: `${theme.colors.red900}`,
              color: "gray600",
            }}
            isLoading={isSubmitting}
            onClick={handleSubmit(deleteStudent)}
          >
            Deletar
          </Button>
          <Button
            width={32}
            type="submit"
            color="white"
            backgroundColor={"blue.500"}
            _hover={{
              backgroundColor: `blue.700`,
              color: "gray600",
            }}
            isLoading={isSubmitting}
            onClick={handleSubmit(updateStudent)}
          >
            Atualizar
          </Button>
        </Flex>
      </Flex>
    </>
  );
};

export default StudentsDetail;
