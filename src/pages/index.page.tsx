import { useEffect } from "react";
import Router from "next/router";

import { Skeleton, Center } from "@chakra-ui/react";
import { theme } from "../styles";

export default function RedirectToHome() {
  useEffect(() => {
    Router.push("/home");
  }, []);

  return (
    <Center height="100%" flexDirection="column" gap={10}>
      <Skeleton
        startColor={theme.colors.gray600.value}
        endColor={theme.colors.gray900.value}
        height="20%"
        width="200px"
        borderRadius={6}
      />
      <Skeleton
        startColor={theme.colors.gray600.value}
        endColor={theme.colors.gray900.value}
        height="5%"
        width="300px"
        borderRadius={6}
      />
    </Center>
  );
}
