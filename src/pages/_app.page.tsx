import type { AppProps } from "next/app";
import { globalStyles } from "../styles/global";
import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { Container, Header, HeaderTitle } from "../styles/pages/app";
import { SideMenu } from "../components/SideMenu";
import { UserInfo } from "../components/UserInfo";
import { useState, useEffect } from "react";

import { MultiSelectTheme } from "chakra-multiselect";

import { SessionProvider } from "next-auth/react";

import Head from "next/head";

import "../lib/firebase";

globalStyles();

function MyApp({ Component, pageProps: { session, ...pageProps } }: AppProps) {
  const [headerHeight, setHeaderHeight] = useState(0);
  const [footerHeight, setFooterHeight] = useState(0);

  useEffect(() => {
    // Calculate header height
    const headerElement = document.getElementById("page-header");
    if (headerElement) {
      setHeaderHeight(headerElement.clientHeight);
    }

    // Calculate footer height
    const footerElement = document.getElementById("page-footer");
    if (footerElement) {
      setFooterHeight(footerElement.clientHeight);
    }
  }, []);

  return (
    <ChakraProvider
      theme={extendTheme({
        components: {
          MultiSelect: {
            ...MultiSelectTheme,
            baseStyle: (props: any) => {
              const baseStyle = MultiSelectTheme.baseStyle(props) as any;
              return {
                ...baseStyle,
                control: {
                  ...baseStyle.control,
                  paddingLeft: 2,
                },
                list: {
                  ...baseStyle.list,
                  bg: "#202024",
                  color: "#E1E1E6",
                  p: 0,
                },
                item: {
                  ...baseStyle.item,
                  _focus: { bg: "#202024" },
                  _active: {
                    bg: "#323238",
                  },
                  _expanded: {
                    bg: "#202024",
                  },
                  _selected: {
                    bg: "#202024",
                  },
                },
                input: {
                  ...baseStyle.input,
                  color: "#E1E1E6",
                },
                selectedItem: {
                  ...baseStyle.selectedItem,
                  borderRadius: 4,
                  bg: "#505059",
                },
                button: {
                  ...baseStyle.button,
                  marginLeft: 2,
                  _hover: {
                    ...baseStyle.button._hover,
                    bg: "#323238",
                    color: "#E1E1E6",
                  },
                  _active: {
                    ...baseStyle.button._active,
                    bg: "#29292E",
                    color: "#E1E1E6",
                  },
                },
              };
            },
          },
        },
      })}
    >
      <SessionProvider session={session}>
        <Container>
          <Head>
            <title>Administration App</title>
          </Head>
          <Header id="page-header">
            <SideMenu />
            <HeaderTitle>Academic Ace Admin</HeaderTitle>
            <UserInfo name="Username" role="Admin" isSideMenu={false} />
          </Header>
          <div
            style={{
              paddingTop: `${headerHeight}px`,
              paddingBottom: `${footerHeight}px`,
              height: "100%",
            }}
          >
            <Component {...pageProps} />
          </div>
        </Container>
      </SessionProvider>
    </ChakraProvider>
  );
}

export default MyApp;
