import { Input } from "@chakra-ui/react";
import { styled } from "../../styles";

export const PasswordStyle = styled(Input, {
  color: "white",
  marginBottom: "30px",
  paddingLeft: "40px !important",
  paddingRight: "16px !important",
  "&::-ms-reveal": {
    display: "none",
  },
});
