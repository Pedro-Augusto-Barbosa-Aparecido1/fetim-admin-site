import {
  Card,
  Flex,
  Text,
  Input,
  Stack,
  Image,
  Button,
  Divider,
  Heading,
  CardBody,
  FormLabel,
  InputGroup,
  CardHeader,
  FormControl,
  InputLeftElement,
  InputRightElement,
  useToast,
} from "@chakra-ui/react";

import { NextPage } from "next";
import { theme } from "../../styles";
import { EmailIcon, LockIcon, ViewOffIcon, ViewIcon } from "@chakra-ui/icons";
import { useRouter } from "next/router";
import React from "react";

import { PasswordStyle } from "./styles";

import { signIn, useSession } from "next-auth/react";
import { z } from "zod";
import { Controller, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { ShieldSlash } from "phosphor-react";

const loginFormSchema = z.object({
  email: z.string().email(),
  password: z.string().min(8),
});

type LoginFormData = z.infer<typeof loginFormSchema>;

interface PasswordInputProps {
  value: string;
  onChange: (value: string) => void;
}

function PasswordInput({ onChange, value }: PasswordInputProps) {
  const [show, setShow] = React.useState(false);
  const handleClick = () => setShow(!show);

  return (
    <InputGroup size="md">
      <InputLeftElement pointerEvents="none">
        <LockIcon color="gray.600" />
      </InputLeftElement>
      <PasswordStyle
        type={show ? "text" : "password"}
        placeholder="Digite sua senha..."
        value={value}
        onChange={onChange}
      />
      <InputRightElement>
        <Button
          onClick={handleClick}
          _hover={{ backgroundColor: "transparent" }}
          variant="ghost"
        >
          {show ? (
            <ViewOffIcon color="white" boxSize="5" />
          ) : (
            <ViewIcon color="white" boxSize="5" />
          )}
        </Button>
      </InputRightElement>
    </InputGroup>
  );
}

const Login: NextPage = () => {
  const router = useRouter();
  const toast = useToast();

  const { status } = useSession();

  if (status === "authenticated") {
    router.push("/home");
  }

  const {
    register,
    control,
    handleSubmit,
    formState: { isSubmitting },
  } = useForm<LoginFormData>({
    resolver: zodResolver(loginFormSchema),
    defaultValues: {
      password: "",
    },
  });

  async function redirect({ email, password }: LoginFormData) {
    toast({
      title: "Entrando...",
      status: "loading",
      isClosable: true,
      position: "top",
      icon: (
        <ShieldSlash color={theme.colors.white.value} weight="bold" size={24} />
      ),
      duration: 1500,
    });

    const credential = await signIn("credentials", {
      redirect: false,
      email,
      password,
    });

    if (credential?.error) {
      return toast({
        title: "Usuário ou senha inválidos",
        status: "error",
        isClosable: true,
        position: "top",
        duration: 3000,
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
      });
    }

    toast({
      title: "Logado com sucesso",
      status: "info",
      isClosable: true,
      position: "top",
      icon: (
        <ShieldSlash color={theme.colors.white.value} weight="bold" size={24} />
      ),
      duration: 3000,
    });

    router.push("/home").then(() => {
      toast({
        title: "Logado com sucesso",
        status: "info",
        isClosable: true,
        position: "top",
        icon: (
          <ShieldSlash
            color={theme.colors.white.value}
            weight="bold"
            size={24}
          />
        ),
        duration: 3000,
      });
    });
  }

  return (
    <Stack
      direction={{
        base: "column",
        sm: "column",
        md: "row",
        lg: "row",
        xl: "row",
      }}
      display="flex"
      alignItems="center"
      justifyContent="space-around"
      height="100%"
      width="100%"
    >
      <Flex
        alignItems="center"
        justifyContent="center"
        flexDirection="column"
        display="flex"
      >
        <Image
          src="/logo.svg"
          alt="logo"
          boxSize={{
            base: "20",
            sm: "24",
            md: "32",
            lg: "36",
            xl: "44",
          }}
        />
        <Text
          fontSize={{
            base: "2xl",
            sm: "4xl",
            md: "4xl",
            lg: "4xl",
            xl: "6xl",
          }}
          color="white"
          fontWeight="bold"
          noOfLines={2}
          textAlign="center"
        >
          Academic Ace
        </Text>
      </Flex>
      <Divider
        position="absolute"
        orientation="vertical"
        height="75%"
        display={{
          base: "none",
          sm: "none",
          md: "flex",
          lg: "flex",
          xl: "flex",
        }}
      />
      <Card
        width={{ sm: "xs", md: "xs", lg: "sm", xl: "md" }}
        ml="4"
        mr="4"
        borderRadius="12px"
        backgroundColor={`${theme.colors.gray900}`}
      >
        <CardHeader
          backgroundColor={`${theme.colors.green500}`}
          borderTopRadius="10px"
          width="100%"
        >
          <Heading
            size="md"
            fontSize="2xl"
            textAlign="center"
            noOfLines={2}
            color="white"
          >
            Faça seu login
          </Heading>
        </CardHeader>
        <CardBody width="100%">
          <FormControl>
            <FormLabel color="white">Email</FormLabel>
            <InputGroup>
              <InputLeftElement pointerEvents="none">
                <EmailIcon color="gray.600" />
              </InputLeftElement>
              <Input
                placeholder="Digite seu email..."
                color="white"
                marginBottom="30px"
                {...register("email")}
              />
            </InputGroup>

            <FormLabel color="white">Senha</FormLabel>
            <Controller
              name="password"
              control={control}
              render={({ field }) => {
                return (
                  <PasswordInput
                    value={field.value}
                    onChange={field.onChange}
                  />
                );
              }}
            />

            <Button
              width="100%"
              color="white"
              backgroundColor={`${theme.colors.green500}`}
              _hover={{
                backgroundColor: `${theme.colors.green800}`,
                color: "gray600",
              }}
              _disabled={{
                brightness: 0.6,
                cursor: "not-allowed",
              }}
              disabled={isSubmitting}
              onClick={handleSubmit(redirect)}
            >
              Entrar
            </Button>
          </FormControl>
        </CardBody>
      </Card>
    </Stack>
  );
};

export default Login;
